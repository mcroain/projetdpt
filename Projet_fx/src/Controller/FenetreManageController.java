package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class FenetreManageController implements Initializable{
	
	public void Entreprise(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionEntreprise.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Entreprise");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Alumni(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionAlumni.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Alumni");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Conference(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionConference.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Conference");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Cours(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionCours.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Cours");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}
	}

	public void Event(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionEvent.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Evenement");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Personne(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionPersonnel.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Personne");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Stagiaire(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionStagiaire.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Stagiaire");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Succursale(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionSuccursale.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Succursale");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void Utilisateur(ActionEvent event)throws IOException 
	{
		try 
		{
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/application/GestionUtilisateur.fxml"));
			AnchorPane root4 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Gestion Utilisateur");
			stage.setScene(new Scene(root4));
			stage.show();
			stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
