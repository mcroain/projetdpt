package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AccueilController implements Initializable {
	
	@FXML
	public AnchorPane rootPane;

	@FXML
	public void signIn(ActionEvent event)throws IOException 
	{
		try 
		{
			AnchorPane fxmlLoader =  FXMLLoader.load(getClass().getResource("PagedeCo.fxml"));
			rootPane.getChildren().setAll(fxmlLoader);
			
			/*
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PagedeCo.fxml"));
			AnchorPane root1 = (AnchorPane) fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Sign In");
			stage.setScene(new Scene(root1));
			stage.show();
			
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Accueil.fxml"));
			Scene scene = new Scene(root,692,384);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setResizable(false);*/
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}