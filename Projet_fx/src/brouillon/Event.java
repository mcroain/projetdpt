package brouillon;

import java.util.Set;
import java.util.HashSet;

/**
* @generated
*/
public class Event {
    
    /**
    * @generated
    */
    private Integer idEvent;
    
    /**
    * @generated
    */
    private String nom;
    
    /**
    * @generated
    */
    private Integer somme;

	public Event(Integer idEvent, String nom, Integer somme) {
		super();
		this.idEvent = idEvent;
		this.nom = nom;
		this.somme = somme;
	}

	public Event() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getSomme() {
		return somme;
	}

	public void setSomme(Integer somme) {
		this.somme = somme;
	}
    
    
   
}
