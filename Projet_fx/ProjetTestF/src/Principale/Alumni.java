package Principale;


/**
* @generated
*/
public class Alumni extends Eleve {
    
    /**
    * @generated
    */
    private String poste;
    
    /**
    * @generated
    */
    private String contact;

	public Alumni(Integer idEleve, String nom, String annee, String niveauEtude, String typeMissionContrat,
			String poste, String contact) {
		super(idEleve, nom, annee, niveauEtude, typeMissionContrat);
		this.poste = poste;
		this.contact = contact;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	
}