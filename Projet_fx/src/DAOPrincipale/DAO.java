package DAOPrincipale;

import java.sql.*;

/**
 * Cette classe generique permet de faire correspondre les interactions entre les differencts type d'objet la BDD.
 * @author marc-eli
 * @param <T> Permet de swap les differents type objet avec lequel on veut interagir.
 */
public abstract class DAO<T> {
  protected Connection connect = null;
   
  /**
   * Constructeur initiant la connexion pour le DAO.
   * @throws SQLException
   */
  public DAO()throws SQLException{
    this.connect = BDDPrincipale.getConnection();
  }
   
  /**
  * Methode permettant d'ajouter un objet passe en parametre a la BDD
  * @param obj qui sera ajoute a la BDD
  * @return boolean true si success/false si fail
  */
  public abstract boolean create(T obj)throws SQLException;

  /**
  * Methode permettant de supprimer un objet passé en parametre de la BDD
  * @param obj qui sera supprimé de la BDD
  * @return boolean true si success/false si fail 
  */
  public abstract boolean delete(T obj)throws SQLException;

  /**
  * Methode permettant de mettre a jour la BDD avec l'objet passe en parametre
  * @param obj qui remplacera l'ancien de la BDD
  * @return boolean true si success/false si fail
  */
  public abstract boolean update(T obj)throws SQLException;

  /**
  * Methode de recherche des informations d'un objet grace a son id(pass en parametre)
  * @param id
  * @return T
  * (Difficultes quant a l'utilisation de la methode qui n'est pas implementee dans la partie javaFX)
  */
  public abstract T find(int id)throws SQLException;
}