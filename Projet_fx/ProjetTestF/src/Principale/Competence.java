package Principale;

public class Competence {

	private Integer idCompetence;
	private String theme;
	private String outil;
	
	
	public Competence(Integer idCompetence, String theme, String outil) {
		super();
		this.idCompetence = idCompetence;
		this.theme = theme;
		this.outil = outil;
	}
	
	public Competence() {
		
	}


	public Integer getIdCompetence() {
		return idCompetence;
	}


	public void setIdCompetence(Integer idCompetence) {
		this.idCompetence = idCompetence;
	}


	public String getTheme() {
		return theme;
	}


	public void setTheme(String theme) {
		this.theme = theme;
	}


	public String getOutil() {
		return outil;
	}


	public void setOutil(String outil) {
		this.outil = outil;
	}
	
	
}
