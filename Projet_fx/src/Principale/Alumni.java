package Principale;

import javafx.scene.control.CheckBox;

/**
* @generated
*/
public class Alumni {
    
    
    
    /**
    * @generated
    */
     private Integer idAlumni;
     
     /**
     * @generated
     */
     private String nom;
     
     /**
     * @generated
     */
     private String annee;
     
     /**
     * @generated
     */
     private String niveauEtude;
     
     /**
     * @generated
     */
     private String typeMissionContrat;
     
     /**
     * @generated
     */
     private String poste;
      
      /**
      * @generated
      */
      private String contact;
      
      private CheckBox check;

	

	public Alumni(Integer idAlumni, String nom, String annee,  String typeMissionContrat,
			String poste, String contact,String niveauEtude) {
		super();
		this.idAlumni = idAlumni;
		this.nom = nom;
		this.annee = annee;
		this.niveauEtude = niveauEtude;
		this.typeMissionContrat = typeMissionContrat;
		this.poste = poste;
		this.contact = contact;
		this.check = new CheckBox();
		
	}
	

		
	public Alumni(){
		
	}
	
	public Integer getIdAlumni() {
		return idAlumni;
	}

	public void setIdAlumni(Integer idAlumni) {
		this.idAlumni = idAlumni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getNiveauEtude() {
		return niveauEtude;
	}

	public void setNiveauEtude(String niveauEtude) {
		this.niveauEtude = niveauEtude;
	}

	public String getTypeMissionContrat() {
		return typeMissionContrat;
	}

	public void setTypeMissionContrat(String typeMissionContrat) {
		this.typeMissionContrat = typeMissionContrat;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public CheckBox getCheck() {
		return check;
	}

	public void setCheck(CheckBox check) {
		this.check = check;
	}
	
}