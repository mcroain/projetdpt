package Controller;

import java.net.URL;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.CoursDAO;
import DAOPrincipale.DAO;
import Principale.Cours;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des Cours
 * @see GestionCours
 * @author marc-eli
 *
 */
public class FenetreCoursController implements Initializable{

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TextField txt_rechercher;
	@FXML
	public TableView<Cours> listCours;
	@FXML
	public TableColumn<Cours, String> col_id;
	@FXML
	public TableColumn<Cours, String> col_matiere;
	@FXML
	public TableColumn<Cours, String> col_filliere;
	@FXML
	public TableColumn<Cours, String> col_annee;
	@FXML
	public TableColumn<Cours, String> col_nbHeure;
	@FXML
	public TableColumn<Cours, String> col_check;
	
	public ObservableList<Cours> data = FXCollections.observableArrayList();
	
	public TextField txt_id;
	public TextField txt_matiere;
	public TextField txt_filliere;
	public TextField txt_annee;
	public TextField txt_nbh;
	
	
	Cours cours =new Cours();
	
	
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		col_id.setCellValueFactory(new PropertyValueFactory<>("idCours"));
		col_matiere.setCellValueFactory(new PropertyValueFactory<>("matiere"));
		col_filliere.setCellValueFactory(new PropertyValueFactory<>("filliere"));
		col_annee.setCellValueFactory(new PropertyValueFactory<>("annee"));
		col_nbHeure.setCellValueFactory(new PropertyValueFactory<>("nombreHeure"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		
		
		
	}
	
	
	
	
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Cours"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute a l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Cours"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listCours.setItems(data);
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM cours");
			while(rs.next()) {
				data.add(new Cours(rs.getInt("idCours"),
									rs.getString("matiere"),
									rs.getString("filliere"),
									rs.getInt("annee"),
									rs.getInt("nbHeure")));									
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("idCours"));
		col_matiere.setCellValueFactory(new PropertyValueFactory<>("matiere"));
		col_filliere.setCellValueFactory(new PropertyValueFactory<>("filliere"));
		col_annee.setCellValueFactory(new PropertyValueFactory<>("annee"));
		col_nbHeure.setCellValueFactory(new PropertyValueFactory<>("nombreHeure"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listCours.setItems(data);
		
	FilteredList<Cours> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(cours -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(cours.getFilliere().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(cours.getMatiere().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getAnnee()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getIdCours()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getNombreHeure()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else
					return false;
			});
			
				});
			SortedList<Cours> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listCours.comparatorProperty());
			listCours.setItems(sorteData);
			sorteData.comparatorProperty().unbind();
			
	
	}
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Cours.<br>
	 * Un nouveau Cours sera créé pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter ce Cours a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see CoursDAO.create();
	 */
	public void ajouter(ActionEvent event)throws SQLException {
		
		 cours = new Cours(null,txt_matiere.getText(),txt_filliere.getText(),Integer.parseInt(txt_annee.getText()),
				 				Integer.parseInt(txt_nbh.getText()));

		 DAO<Cours> add = new CoursDAO();
		 add.create(cours);
	}
	
	
	
	/**
	 * Methode permettant de supprimer un cours de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see CoursDAO.delete()
	 */
	
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Cours> dataRemove = FXCollections.observableArrayList();		
		 DAO<Cours> clean = new CoursDAO();
		 for(Cours cours : data) {
			 if(cours.getCheck().isSelected()) {
				
				 dataRemove.add(cours);
				 clean.delete(cours);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	/**
	 * Methode permettant de mettre a jour un element de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox a ete cochee seront modifies en consequence<br>
	 * @param event
	 * @throws SQLException
	 * @see CoursDAO.update()
	 */
	
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Cours> dataUpdate = FXCollections.observableArrayList();		
		 DAO<Cours> maj = new CoursDAO();
		 
		 
		 for(Cours cours : data) {
			 if(cours.getCheck().isSelected()) {
				 Cours alter = new Cours(cours.getIdCours(),txt_matiere.getText(),txt_filliere.getText(),
							Integer.parseInt(txt_annee.getText()),Integer.parseInt(txt_nbh.getText()));
				 maj.update(alter);
				 
			 }
		 }
	}
	
	@FXML
	public void search(KeyEvent event) {
		FilteredList<Cours> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(cours -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(cours.getFilliere().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(cours.getMatiere().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getAnnee()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getIdCours()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(cours.getNombreHeure()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else
					return false;
			});
			
				});
			SortedList<Cours> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listCours.comparatorProperty());
			listCours.setItems(sorteData);
	}
	
}
