package Principale;


public class Resultats {

	private String res1;
	private String res2;
	private String res3;
	
	public Resultats (String res1, String res2, String res3) {
		super();
		this.res1 = res1;
		this.res2 = res2;
		this.res3 = res3;
	}
	
	public Resultats() {
		// TODO Auto-generated constructor stub
	}

	public String getRes1() {
		return res1;
	}

	public void setRes1(String res1) {
		this.res1 = res1;
	}

	public String getRes2() {
		return res2;
	}

	public void setRes2(String res2) {
		this.res2 = res2;
	}

	public String getRes3() {
		return res3;
	}

	public void setRes3(String res3) {
		this.res3 = res3;
	}

}
