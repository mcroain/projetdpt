module ProjectGraphique {


	exports application;
	exports Controller;
	exports Principale;
	exports Connexion;
	exports DAOPrincipale;

	requires javafx.controls;
	requires javafx.media;
	requires javafx.swing;
	requires javafx.web;
	requires javafx.base;
	requires javafx.fxml;
	requires javafx.graphics;
	requires java.sql;
}