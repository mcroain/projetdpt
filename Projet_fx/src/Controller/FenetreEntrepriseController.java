package Controller;

import java.io.IOException;


import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.Set;

import Connexion.BDDConnexion;
import Connexion.DAOUtilisateur;
import Connexion.Utilisateurs;
import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.EntrepriseDAO;
import Principale.Cours;
import Principale.Entreprise;
import Principale.Resultats;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des Entreprises.
 * @see GestionEntreprise
 * @author marc-eli
 *
 */
public class FenetreEntrepriseController implements Initializable {

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Entreprise> listCie;
	@FXML
	public TableColumn<Entreprise, String> col_id;
	@FXML
	public TableColumn<Entreprise, String> col_rs;
	@FXML
	public TableColumn<Entreprise, String> col_den;
	@FXML
	public TableColumn<Entreprise, String> col_ad;
	@FXML
	public TableColumn<Entreprise, String> col_Sa;
	@FXML
	public TableColumn<Entreprise, String> col_site;
	@FXML
	public TableColumn<Entreprise, String> col_plage;
	@FXML
	public TableColumn<Entreprise, String> col_check;
	
	public ObservableList<Entreprise> data = FXCollections.observableArrayList();
	
	
	public TextField txtId;
	public TextField txtRS;
	public TextField txtDenomination;
	public TextField txtAdSiege;
	public TextField txtAct;
	public TextField txtSite;
	public TextField txtCom;
	public TextField txtPlage;
	public TextField txtSearch;

	Entreprise cie = new Entreprise();
	
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Entreprise"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute à l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Entreprise"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listCie.getItems().clear();
	
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM Entreprise");
			while(rs.next()) {
				data.add(new Entreprise(rs.getInt("idEntreprise"),
										rs.getString("raisonSociale"),
										rs.getString("denomination"),
										rs.getString("adresseSiege"),
										rs.getString("secteurActivite"),
										rs.getString("site")));
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
			 			
		col_id.setCellValueFactory(new PropertyValueFactory<>("idEntreprise"));
		col_rs.setCellValueFactory(new PropertyValueFactory<>("raisonSociale"));
		//col_den.setCellValueFactory(new PropertyValueFactory<>("denomination")); 
		col_ad.setCellValueFactory(new PropertyValueFactory<>("adresseSiege"));
		col_Sa.setCellValueFactory(new PropertyValueFactory<>("secteurActivite"));
		col_site.setCellValueFactory(new PropertyValueFactory<>("site"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		
		 listCie.setItems(data);

		 FilteredList<Entreprise> filterData = new FilteredList<>(data, b -> true);
			txtSearch.textProperty().addListener((observable, oldValue,newValue) -> {
				filterData.setPredicate(cie -> {
					
					if(newValue == null || newValue.isEmpty()) {
						return true;
					}
					
					String lowerCaseFilter = newValue.toLowerCase();
					
					System.out.println(cie.getAdresseSiege());
					if(cie.getAdresseSiege().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					else if(cie.getRaisonSociale().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					else if(String.valueOf(cie.getIdEntreprise()).indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					if(cie.getRaisonSociale().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					if(cie.getSecteurActivite().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					if(cie.getSite().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
					}
					else
						return false;
				});
				
					});
				SortedList<Entreprise> sorteData = new SortedList<>(filterData);
				sorteData.comparatorProperty().bind(listCie.comparatorProperty());
				listCie.setItems(sorteData);
		 
		    
	}
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Entreprises.<br>
	 * Une nouvelle Entreprise sera créé pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cette Entreprise a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see EntrepriseDAO.create();
	 */
	
	public void ajouter(ActionEvent event)throws SQLException {
		
			 cie = new Entreprise(null,txtRS.getText(),null,txtAdSiege.getText(),
											txtAct.getText(),txtSite.getText());
	
			 EntrepriseDAO add = new EntrepriseDAO();
			 add.create(cie);
			 

		
		
	}
	
	/**
	 * Methode permettant de supprimer une Entreprise de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see EntrepriseDAO.delete()
	 */
	
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Entreprise> dataRemove = FXCollections.observableArrayList();		
		 EntrepriseDAO clean = new EntrepriseDAO();
		 for(Entreprise cie : data) {
			 if(cie.getCheck().isSelected()) {
				
				 dataRemove.add(cie);
				 clean.delete(cie);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	/**
	 * Methode permettant de mettre a jour un element de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox à ete cochee seront modifies en conséquent<br>
	 * @param event
	 * @throws SQLException
	 * @see EntrepriseDAO.update()
	 */
	
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Entreprise> dataUpdate = FXCollections.observableArrayList();		
		 EntrepriseDAO maj = new EntrepriseDAO();
		 
		 
		 for(Entreprise cie : data) {
			 if(cie.getCheck().isSelected()) {
				 Entreprise alter = new Entreprise(cie.getIdEntreprise(),txtRS.getText(),null,txtAdSiege.getText(),
							txtAct.getText(),txtSite.getText());
				 maj.update(alter);
			 }
		 }
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}