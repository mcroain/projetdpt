package DAOPrincipale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

public class CoursDAO extends DAO<Cours>{

	public CoursDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO cours VALUES (NULL, ?, ?, ?, ?)");
	            ps.setString(1, obj.getMatiere());
	            ps.setString(2, obj.getFilliere());
	            ps.setString(3, obj.getAnnee());
	            ps.setInt(4, obj.getNombreHeure());
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		return false;
	}

	@Override
	public boolean delete(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM cours WHERE idCours=" + obj.getIdCours());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public boolean update(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE cours SET matiere=?, filliere=?, annee =?, nombreHeure=? WHERE idCours=?");
	            ps.setString(1, obj.getMatiere());
	            ps.setString(2, obj.getFilliere());
	            ps.setString(3, obj.getAnnee());
	            ps.setInt(4, obj.getNombreHeure());
	            ps.setInt(5, obj.getIdCours());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public Cours find(int id) throws SQLException {
		Cours cours = new Cours();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM cours  WHERE idCours=" + id);
            if(rs.next())
            {
            	cours = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return cours;
		
	}
	
	public static Set getAllCours() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication");
            Set cours = new HashSet();
            while(rs.next())
            {
                Cours c = getResult(rs);
                cours.add(c);
            }
            return cours;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
	
	
	public static Cours getResult(ResultSet rs) throws SQLException {
        Cours cours = new Cours();
        cours.setIdCours( rs.getInt("idCours") );
        cours.setMatiere( rs.getString("matiere") );
        cours.setFilliere( rs.getString("filliere") );
        cours.setAnnee( rs.getString("annee") );
        cours.setNombreHeure( rs.getInt("nomberHeure") );
       
        return cours;
    }

}
