package DAOPrincipale;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Cette classe permet d'initier la connexion ? la BDD
 * @author marc-eli, corentin, maxime
 * @version 1.0
 */

public class BDDPrincipale {
	private static String URL = "jdbc:"
			+ "mysql://localhost:3306/projet_l2" +
			"?useSSL=false&useUnicode=true"+
					"&useJDBCCompliantTimezoneShift=true"+
			"&useLegacyDatetimeCode=false"+
					"&serverTimezone=UTC"+
			"&allowPublicKeyRetrieval=true";
	private static String User = "root";
	private static String Password = "";
	private static Connection init;

	public BDDPrincipale() {
	
	}
	
	
	
	
	/**
	 * Methode permettant de se connecter ? la BDD
	 * @return Le statut de la connexion (connect?)
	 * @throws SQLException
	 */
	
	public static Connection getConnection()throws SQLException {
	    try {
	    	Connection connect = DriverManager.getConnection(URL,User,Password);
	    	return connect;
	    }
	    catch(SQLException e) {
	    	throw new RuntimeException("Probleme de connexion ? la BDD", e);
	    }
	

}
}
