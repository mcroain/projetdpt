package DAOPrincipale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

public class PersonnelDAO extends DAO<Personnel> {

	public PersonnelDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Personnel obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try { 	
            PreparedStatement ps = connection.prepareStatement("INSERT INTO communication VALUES (NULL, ?, ?, ?)");
            ps.setString(1, obj.getNom());
            ps.setString(2, obj.getFonction()); 
            ps.setInt(3, obj.getIdEntreprise());
            int i = ps.executeUpdate();
          if(i == 1) {    
            return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public boolean delete(Personnel obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM personnel WHERE idPersonnel=" + obj.getIdPersonnel());
          if(i == 1) {
        return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public boolean update(Personnel obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE personnel SET nom=?, fonction=? WHERE idPersonnel=?");
            ps.setString(1, obj.getNom());
            ps.setString(2, obj.getFonction());
            ps.setInt(3, obj.getIdPersonnel());
            
            int i = ps.executeUpdate();
          if(i == 1) {
        return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public Personnel find(int id) throws SQLException {
		Personnel pers = new Personnel();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM personnel  WHERE idPersonnel=" + id);
            if(rs.next())
            {
            	pers = getResult(rs);
            	
            	rs = this.connect.createStatement().executeQuery("SELECT c.theme, c.outil FROM personnel pe INNER JOIN possede p"
            			+ "ON p.idPersonnel = pe.idPersonnel INNER JOIN competence c"
						+ "ON c.idCompetence = p.idCompetence "					
						+ "AND c.idCompetence =" +id);
            	
            	CompetenceDAO comp = new CompetenceDAO();
            	
            	while(rs.next()) 
            		
            		pers.addCompetence(comp.find(rs.getInt("c.idCompetence")));   
            	
            	ConferenceDAO conf = new ConferenceDAO();
            	
            	rs = this.connect.createStatement().executeQuery("SELECT c.theme, c.public, c.duree, a.dateConf FROM personnel pe INNER JOIN animer a"
            			+ "ON p.idpersonnel = a.idPersonnel INNER JOIN conference c"
						+ "ON c.idConference = a.idConference "					
						+ "AND idPersonnel =" +id);
            	
            	while(rs.next())
            		
            		pers.addConference(conf.find(rs.getInt("")));
            	
            	CoursDAO cours = new CoursDAO();
            	
            	rs = this.connect.createStatement().executeQuery("SELECT c.matiere, c.filliere, c.annee, c.nbHeure FROM personnel pe INNER JOIN donner d"
            			+ "ON p.idpersonnel = d.idPersonnel INNER JOIN cours c"
						+ "ON c.idCours = d.idCours "					
						+ "AND idPersonnel =" +id);
            	
            	while(rs.next())
            		
            		pers.addCours(cours.find(rs.getInt("")));
            	
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return pers;
		
	}
	
		
	
	
	
	public static Set getAllPersonnel() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM personnel");
            Set personnel = new HashSet();
            while(rs.next())
            {
                Personnel pers = getResult(rs);
                personnel.add(pers);
            }
            return personnel;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
	
	
	 public static Personnel getResult(ResultSet rs) throws SQLException {
	        Personnel pers = new Personnel();
	        pers.setIdPersonnel( rs.getInt("idPersonnel") );
	        pers.setNom( rs.getString("nom") );
	        pers.setFonction( rs.getString("fonction") );
	        pers.setIdEntreprise(rs.getInt("idEntreprise"));
	       
	        return pers;
	    }

}
