package Controller;


import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Connexion.BDDConnexion;
import Connexion.DAOUtilisateur;
import Connexion.Utilisateurs;
import DAOPrincipale.DAO;
import Principale.Cours;
import Principale.Succursale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class FenetreUtilisateurController implements Initializable{

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Utilisateurs> listUser;
	@FXML
	public TableColumn<Utilisateurs, String> col_id;
	@FXML
	public TableColumn<Utilisateurs, String> col_nom;
	@FXML
	public TableColumn<Utilisateurs, String> col_pass;
	@FXML
	public TableColumn<Utilisateurs, String> col_type;	
	@FXML
	public TableColumn<Utilisateurs, String> col_check;
	
	public ObservableList<Utilisateurs> data = FXCollections.observableArrayList();
	
	
	public TextField txt_id;
	public TextField txt_nom;
	public TextField txt_password;
	public TextField txt_type;
	public TextField txt_rechercher;
	public RadioButton btnUser;
	public RadioButton btnAdmin;
	
	Utilisateurs user = new Utilisateurs();
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "utilisateur"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute à l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Utilisateur"
	 */
	
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listUser.getItems().clear();
		
		Connection con = BDDConnexion.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT u.idUtilisateur,u.nom,u.password,t.nom  FROM utilisateur u "
															+ "INNER JOIN type t ON u.idType = t.idType");
			while(rs.next()) {
				data.add(new Utilisateurs(rs.getInt("u.idUtilisateur"),
									rs.getString("u.nom"),
									rs.getString("u.password"),
									rs.getString("t.nom"),
													null));
																		
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
		col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		col_pass.setCellValueFactory(new PropertyValueFactory<>("password"));
		col_type.setCellValueFactory(new PropertyValueFactory<>("type"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listUser.setItems(data);
		
		FilteredList<Utilisateurs> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(user -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(user.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				else if(String.valueOf(user.getId()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(user.getPassword().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				else
					return false;
			});
			
				});
			SortedList<Utilisateurs> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listUser.comparatorProperty());
			listUser.setItems(sorteData);
	}
	
	/**
	 * Methode permettant d'ajouter des elements à la BDD correspondant aux Utilisateurs.<br>
	 * Un nouvel utilisateur sera cree pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cet Utilisateur a la DB<br>
	 * L'idType sera modifie en fonction d'un radio button coche par l'uitlisateur (setIdType 1 si Admin, setIdType 2 si User)
	 * @param event
	 * @throws SQLException
	 * @see UtilisateurDAO.create();
	 */
	
	public void ajouter(ActionEvent event)throws SQLException {
		
		 user = new Utilisateurs(null,txt_nom.getText(),txt_password.getText(),null,null);
		 boolean check = false;
			while(!check) {
			if(btnAdmin.isSelected()) {
				user.setIdType(1);
				check = true;
				
			}
			else if (btnUser.isSelected()) {
				user.setIdType(2);
				check = true;
			}
			}
		 
		 DAOUtilisateur add = new DAOUtilisateur();
		 add.create(user);
	}
	
	
	/**
	 * Methode permettant de supprimer un utilisateur de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see UtilisateurDAO.delete()
	 */
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Utilisateurs> dataRemove = FXCollections.observableArrayList();		
		 DAOUtilisateur clean = new DAOUtilisateur();
		 for(Utilisateurs user : data) {
			 if(user.getCheck().isSelected()) {
				
				 dataRemove.add(user);
				 clean.remove(user);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	
	/**
	 * Methode permettant de mettre a jour un élément de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox à été cochée seront modifiés en conséquence<br>
	 * @param event
	 * @throws SQLException
	 * @see Succursale.update()
	 */
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Utilisateurs> dataUpdate = FXCollections.observableArrayList();		
		 DAOUtilisateur maj = new DAOUtilisateur();
		 
		 
		 for(Utilisateurs user : data) {
			 if(user.getCheck().isSelected()) {
				 Utilisateurs alter = new Utilisateurs(user.getId(),txt_nom.getText(),txt_password.getText(),
														null,null);
				 boolean check = false;
					while(!check) {
					if(btnAdmin.isSelected()) {
						btnUser.setDisable(true);
						user.setIdType(1);
						check = true;
						
					}
					else if (btnUser.isSelected()) {
						btnAdmin.setDisable(true);
						user.setIdType(2);
						check = true;
					}
					}
				 maj.update(alter);
			 }
		 }
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
}
