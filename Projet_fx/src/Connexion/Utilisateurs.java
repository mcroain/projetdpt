package Connexion;

import javafx.scene.control.CheckBox;

/**
 * Cette classe permet de creer l'objet "utilisateur"
 * @author marc-eli, corentin, maxime
 * @version 1.0
 */
public class Utilisateurs {

	private Integer id;
	private String nom;
	private String password;
	private String type;
	private CheckBox check;
	private Integer idType;

	/**
	 * Constructeur par defaut
	 */
	public Utilisateurs() {}
	
	/**
	 * Construit un utilisateur avec un id, un nom, un mot de passe et un type.<br>
	 * Parametres conformes a la BDD
	 * @param id = id de l'utilisateur
	 * @param nom = pseudo de l'utilisateur (ou nom selon l'appellation)
	 * @param password = mot de passe de l'utilisateur
	 * @param type = type de l'utilisateur (admin ou utilisateur lambda)
	 */
	public Utilisateurs(Integer id, String nom, String password, String type,Integer idType) {
		super();
		this.id = id;
		this.nom = nom;
		this.password = password;
		this.type = type;
		this.idType = idType;
		this.check = new CheckBox();
	}

	

	/**
	 * Pour obtenir l'identifiant de l'utilisateur
	 * @return l'id de l'utilisateur
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Modifie l'identifiant de l'utilisateur avec l'argument passe en parametre
	 * @param id = nouvel identifiant
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	/**
	 * Pour obtenir le type de l'utilisateur
	 * @return le type de l'utilisateur
	 */
	public String getType() {
		return type;
	}
	/**
	 * Modifie le type de l'utilisateur avec l'argument passe en parametre
	 * @param type = nouveau type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Pour obtenir le pseudo/nom de l'utilisateur
	 * @return le nom de l'utilisateur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Modifie le nom/pseudo de l'utilisateur avec l'argument passe en parametre
	 * @param pseudo = nouveau nom/pseudo de l'utilisateur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Pour obtenir le mot de passe de l'utilisateur
	 * @return le mot de passe de l'utilisateur
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Modifie le mot de passe de l'utilisateur avec l'argument passe en parametre
	 * @param password = nouveau mot de passe
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Pour obtenir l'idType de l'utilisateur
	 * @return l'idType de l'utilisateur
	 */
	public Integer getIdType() {
		return idType;
	}

	/**
	 * Modifie l'idType de l'utilisateur avec l'argument passe en parametre
	 * @param idType = nouveau idType
	 */
	public void setIdType(Integer idType) {
		this.idType = idType;
	}
	
	
	/**
	 * 
	 * @param check = nouvelle checkBox
	 */
	
	public CheckBox getCheck() {
		return check;
	}

	/**
	 * Modifie la checkBox. Cette deniere servira a interagir avec la tableView
	 * @param check = nouvelle checkBox
	 */
	
	public void setCheck(CheckBox check) {
		this.check = check;
	}
	
	
	
}
