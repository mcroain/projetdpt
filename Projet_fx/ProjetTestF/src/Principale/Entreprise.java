package Principale;

import java.util.Set;
import java.util.HashSet;

/**
* @generated
*/
public class Entreprise {
    
    
    private Integer idEntreprise;
    private String raisonSocial;
    private String denomination;
    private String adresseSiege;
    private String secteurActivite;   
    private String site; 
    private Set<Communication>listCom;
    private Set<Succursale>listSuc;
    private Set<Stagiaire>listStagiaire;
    private Set<Alumni>listAlumni;
    private Set<Event>listEvent;
    
    
    
    
	public Entreprise(Integer idEntreprise, String raisonSocial, String denomination, String adresseSiege,
			String secteurActivite, String site) {
		super();
		this.idEntreprise = idEntreprise;
		this.raisonSocial = raisonSocial;
		this.denomination = denomination;
		this.adresseSiege = adresseSiege;
		this.secteurActivite = secteurActivite;
		this.site = site;
	}

	
	public Entreprise() {
	}
	
	
	


	public Integer getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Integer idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getAdresseSiege() {
		return adresseSiege;
	}

	public void setAdresseSiege(String adresseSiege) {
		this.adresseSiege = adresseSiege;
	}

	public String getSecteurActivite() {
		return secteurActivite;
	}

	public void setSecteurActivite(String secteurActivite) {
		this.secteurActivite = secteurActivite;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}


	public Set<Communication> getListCom(){
		return listCom;
	}
	
	public void setListCom(Set<Communication> listCom) {
		this.listCom = listCom;
	}

	
	public void addCommunication(Communication com){
	    this.listCom.add(com);
	}
	  
	public void removeCommunication(Communication com){
		    this.listCom.remove(com);
	}
	
	
	public Set<Succursale> getListSuc(){
		return listSuc;
	}
	
	public void setListSuc(Set<Succursale> listSuc) {
		this.listSuc = listSuc;
	}
	
	public void addSuccursale(Succursale suc){
	    this.listSuc.add(suc);
	}
	  
	public void removeSuccursale(Succursale suc){
		    this.listSuc.remove(suc);
	}

	public Set<Stagiaire> getListStagiaire(){
		return listStagiaire;
	}
	
	public void setListStagiaire(Set<Stagiaire> listStagiaire) {
		this.listStagiaire = listStagiaire;
	}
	
	public void addStagiaire(Stagiaire stagiaire){
	    this.listStagiaire.add(stagiaire);
	}
	  
	public void removeStagiaire(Stagiaire stagiaire){
		    this.listStagiaire.remove(stagiaire);
	}

	public Set<Alumni> getListAlumni(){
		return listAlumni;
	}
	
	public void setListAlumni(Set<Alumni> listAlumni) {
		this.listAlumni = listAlumni;
	}

	public void addAlumni(Alumni alumni){
	    this.listAlumni.add(alumni);
	}
	  
	public void removeAlumni(Alumni alumni){
		    this.listAlumni.remove(alumni);
	}
	
	public Set<Event> getListEvent(){
		return listEvent;
	}
	public void setListEvent(Set<Event> listEvent) {
		this.listEvent = listEvent;
	}
    
	public void addEvent(Event event){
	    this.listEvent.add(event);
	}
	  
	public void removeEvent(Event event){
		    this.listEvent.remove(event);
	}
	
	
	
	
	
	
	
    
}