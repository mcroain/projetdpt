package brouillon;

import java.io.IOException;
import java.sql.*;

import Connexion.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SampleController {

	public TextField txtUserName;
	public TextField txtPass;
	public Label connectStatus;
	
	
	public void login(ActionEvent actionEvent) throws SQLException {
		Connexion.DAOUtilisateur.getUserByNameAndPassword( txtUserName.getText(), txtPass.getText(),connectStatus );
	}
	
	public void signUp(ActionEvent actionEvent)throws IOException {
		Stage primaryStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/application/SignUp.fxml"));
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	
}
