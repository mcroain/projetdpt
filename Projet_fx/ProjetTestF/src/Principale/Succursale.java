package Principale;


/**
* @generated
*/
public class Succursale extends Entreprise {
    
    /**
    * @generated
    */
    private String nom;
    
    /**
    * @generated
    */
    private String localisation;

	public Succursale(String nom, String localisation) {
		super();
		this.nom = nom;
		this.localisation = localisation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
     
    
}
