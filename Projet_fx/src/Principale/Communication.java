package Principale;

import java.util.Set;
import java.util.HashSet;

/**
* @generated
*/
public class Communication {
    
    /**
    * @generated
    */
    private Integer idCommunication;
    
    /**
    * @generated
    */
    private String type;
    
    /**
    * @generated
    */
    private String plage;

	public Communication(Integer idCommunication, String type, String plage) {
		super();
		this.idCommunication = idCommunication;
		this.type = type;
		this.plage = plage;
	}
	
	public Communication() {
		
	}

	public Integer getIdCommunication() {
		return idCommunication;
	}

	public void setIdCommunication(Integer idCommunication) {
		this.idCommunication = idCommunication;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlage() {
		return plage;
	}

	public void setPlage(String plage) {
		this.plage = plage;
	}
    
    
}