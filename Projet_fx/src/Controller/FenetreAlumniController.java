package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.AlumniDAO;
import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.DAO;
import Principale.Alumni;
import Principale.Cours;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des alumni
 * @see GestionAlumni
 * @author marc-eli
 *
 */

public class FenetreAlumniController implements Initializable{


	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Alumni> listAlumni;
	@FXML
	public TableColumn<Alumni, String> col_id;
	@FXML
	public TableColumn<Alumni, String> col_nom;
	@FXML
	public TableColumn<Alumni, String> col_annee;
	@FXML
	public TableColumn<Alumni, String> col_contrat;
	@FXML
	public TableColumn<Alumni, String> col_poste;
	@FXML
	public TableColumn<Alumni, String> col_contact;
	@FXML
	public TableColumn<Alumni, String> col_check;
	
	public ObservableList<Alumni> data = FXCollections.observableArrayList();
	
	public TextField txt_id;
	public TextField txt_nom;
	public TextField txt_annee;
	public TextField txt_contrat;
	public TextField txt_poste;
	public TextField txt_contact;
	public TextField txt_rechercher;
	
	public RadioButton btnPhone;
	public RadioButton btnMail;
	public RadioButton btnAutre;
	
	Alumni alumni =new Alumni();
	
	
	
	
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Alumni"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute a l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Alumni"
	 */
	
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listAlumni.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT a.idAlumni, a.nom,fe.annee,a.typeContrat,a.poste,ca.plage\r\n" + 
					"FROM alumni a \r\n" + 
					"LEFT OUTER JOIN ficheetude fe ON a.idFicheEtude = fe.idFicheEtude\r\n" + 
					"LEFT OUTER JOIN communique_a ca ON ca.idAlumni = a.idAlumni\r\n" + 
					"LEFT OUTER JOIN communication c ON ca.idCommunication = c.idCommunication ");
			while(rs.next()) {
				data.add(new Alumni(rs.getInt("a.idAlumni"),
									rs.getString("a.nom"),
									rs.getString("fe.annee"),
									rs.getString("a.typeContrat"),
									rs.getString("a.poste"),
									rs.getString("ca.plage"),
									null));									
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("idAlumni"));
		col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		col_annee.setCellValueFactory(new PropertyValueFactory<>("annee"));
		col_contrat.setCellValueFactory(new PropertyValueFactory<>("typeMissionContrat"));
		col_poste.setCellValueFactory(new PropertyValueFactory<>("poste"));
		col_contact.setCellValueFactory(new PropertyValueFactory<>("contact"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listAlumni.setItems(data);
		
		FilteredList<Alumni> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(alumni -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				
				 if(String.valueOf(alumni.getIdAlumni()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(alumni.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(alumni.getPoste().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(alumni.getTypeMissionContrat().toLowerCase().indexOf(lowerCaseFilter) != -1) {
						return true;
				}
				else
					return false;
			});
			
				});
			SortedList<Alumni> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listAlumni.comparatorProperty());
			listAlumni.setItems(sorteData);
	}
	
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Alumni.<br>
	 * Un nouvel alumnus sera créé pour lui attribuer les valeurs des TextField<br>
	 * On se sert d'un radio button pour transformer le type "String" en type "Int", et ainsi pouvoir attribuer un type matchant avec l'idAlumni.<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cet alumnus e la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see AlumniDAO.create();
	 */
	
	public void ajouter(ActionEvent event)throws SQLException {
		
		 alumni = new Alumni(null,txt_nom.getText(),null,txt_contrat.getText(),txt_poste.getText(),null,null);
		Integer res;
		 boolean check = false;
			
			if(btnPhone.isSelected()) {
				res = 1;
				
				
			}
			else if (btnMail.isSelected()) {
				res= 2;
				
			}
			else {
				res =3;
			}
			System.out.println(res);
			
			

		 DAO<Alumni> add = new AlumniDAO();
		 add.create(alumni);
		 
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	        	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO ficheEtude VALUES (NULL,NULL, ?)");
	            ps.setString(1, txt_annee.getText());
	            ps.executeUpdate();
	            
	            ps = connection.prepareStatement("INSERT INTO communication_A VALUES("+res+","+alumni.getIdAlumni()+","+
	            									txt_contact.getText()+")");
	            
	            
	          
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	}
	
	
	/**
	 * Methode permettant de supprimer un alumnus de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see AlumniDAO.delete()
	 */
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Alumni> dataRemove = FXCollections.observableArrayList();		
		 DAO<Alumni> clean = new AlumniDAO();
		 for(Alumni alumni : data) {
			 if(alumni.getCheck().isSelected()) {
				
				 dataRemove.add(alumni);
				 clean.delete(alumni);
			 }
		 }
		 data.removeAll(dataRemove);
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
