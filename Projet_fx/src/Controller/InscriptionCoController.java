package application;

import java.io.IOException;

import java.sql.SQLException;

import Connexion.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class InscriptionCoController 
{
	public TextField txtUserName;
	public TextField txtPass;
	public RadioButton btnAdmin;
	public RadioButton btnUser;
	
	public void signUp(ActionEvent event)throws IOException, SQLException{
		
		Utilisateurs user= new Utilisateurs(null,txtUserName.getText(),txtPass.getText(),null);
		boolean check = false;
		while(!check) {
		if(btnAdmin.isSelected()) {
			user.setType(1);
			check = true;
			
		}
		else if (btnUser.isSelected()) {
			user.setType(2);
			check = true;
		}
		}
		DAOUtilisateur.addUtilisateur(user);
		

		/*
		Stage primaryStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/application/SignUp.fxml"));
		Scene scene = new Scene(root,400,400);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show(); */
	}
}
