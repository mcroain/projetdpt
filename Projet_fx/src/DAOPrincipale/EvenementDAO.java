package DAOPrincipale;

import java.sql.Connection;


import Principale.*;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;

/**
 * Classe DAO pour les competences
 * @author maxime,marc-eli
 * @see DAO 
 * @see Evenement
 */
public class EvenementDAO extends DAO<Evenement> {

	public EvenementDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	@Override
	public boolean create(Evenement obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	        	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO evenement VALUES (NULL, ?, ?)");
	            ps.setInt(1, obj.getSomme());
	            ps.setString(2, obj.getNom());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Evenement obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM evenement WHERE idEvenement=" + obj.getIdEvent());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Evenement obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE evenement SET nom=?, somme=? WHERE idEvenement="+obj.getIdEvent());
	            ps.setString(1, obj.getNom());
	            ps.setInt(2, obj.getSomme());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}
	
	/**

	 * @see DAOPrincipale.DAO#find(int)
	 */
	public Evenement find(int id)throws SQLException {
		Evenement event = new Evenement();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM event  WHERE idEvent=" + id);
            if(rs.next())
            {
            	event = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return event;
	}
	
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return L'Evenement avec les valeurs de la requete
	 * @throws SQLException
	 */
	 public static Evenement getResult(ResultSet rs) throws SQLException {
	        Evenement event = new Evenement();
	        event.setIdEvent( rs.getInt("idEvent") );
	        event.setNom( rs.getString("nom") );
	        event.setSomme( rs.getInt("somme") );
	       
	        return event;
	    }
	
		
}