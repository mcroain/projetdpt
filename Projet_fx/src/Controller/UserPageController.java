package Controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import DAOPrincipale.BDDPrincipale;
import Principale.Resultats;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class UserPageController implements Initializable {

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Resultats> listRes;
	@FXML
	public TableColumn<Resultats, String> res1;
	@FXML
	public TableColumn<Resultats, String> res2;
	@FXML
	public TableColumn<Resultats, String> res3;
	
	public ObservableList<Resultats> data = FXCollections.observableArrayList();
	
	public TextField txt_rechercher;
	
	Resultats res = new Resultats();
	
	/**
	 * Permet de changer de scene sans en ouvrir une nouvelle
	 * @param event
	 * @throws IOException
	 */
	
	

	
	
	/**
	 * On recherche tous les résultats de la vue "resultatEntreprises", destinés aux "Simples" Utilisateurs.
	 * @param event
	 * @throws SQLException
	 * @see Class "/Principale/Resultats"
	 */
	public void resultEntreprise(ActionEvent event)throws SQLException {
		listRes.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM resultatentreprise");
			while(rs.next()) {
				data.add(new Resultats(rs.getString("nom"), rs.getString("Adresse"), rs.getString("contact")));
			}
			
			 
			
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
			
		
		res1.setCellValueFactory(new PropertyValueFactory<>("res1"));
		 res2.setCellValueFactory(new PropertyValueFactory<>("res2"));
		 res3.setCellValueFactory(new PropertyValueFactory<>("res3")); 
		    
		 listRes.setItems(data);
		    
		
		 
	}
	
	/**
	 * On recherche tous les résultats de la vue "resultatEvent", destinés aux "Simples" Utilisateurs.
	 * @param event
	 * @throws SQLException
	 * @see Class "/Principale/Resultats"
	 */
	public void resultEvent(ActionEvent event)throws SQLException {
		listRes.getItems().clear();
		Connection con = BDDPrincipale.getConnection();
		try {
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM resultatevent" );
			while(rs.next()) {
			data.add(new Resultats(rs.getString("nom"), rs.getString("prix"), rs.getString("participant")));
			}
		}
		catch (SQLException ex) {
	        ex.printStackTrace();
	        }
		
		res1.setCellValueFactory(new PropertyValueFactory<>("res1"));
		 res2.setCellValueFactory(new PropertyValueFactory<>("res2"));
		 res3.setCellValueFactory(new PropertyValueFactory<>("res3")); 
		
		listRes.setItems(data);
	}
	
	/**
	 * On recherche tous les résultats de la vue "resultatPersonnel", destinés aux "Simples" Utilisateurs.
	 * @param event
	 * @throws SQLException
	 * @see Class "/Principale/Resultats"
	 */
		
	public void resultPersonnel(ActionEvent event)throws SQLException {
		listRes.getItems().clear();
		Connection con = BDDPrincipale.getConnection();
		try {
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM resultatpersonnel" );
			while(rs.next()) {
			data.add(new Resultats(rs.getString("nom"), rs.getString("fonction"), rs.getString("contact")));
			}
		}
		catch (SQLException ex) {
	        ex.printStackTrace();
	        }
		
		res1.setCellValueFactory(new PropertyValueFactory<>("res1"));
		 res2.setCellValueFactory(new PropertyValueFactory<>("res2"));
		 res3.setCellValueFactory(new PropertyValueFactory<>("res3")); 
		
		listRes.setItems(data);
	}
	
	
	public void getAll(ActionEvent event)throws SQLException {
		listRes.getItems().clear();
		 Connection connection = BDDPrincipale.getConnection();
         try {
             Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM resultatentreprise UNION SELECT * FROM resultatpersonnel UNION SELECT * FROM resultatevent" );
             while(rs.next())
             {   	
             data.add(new Resultats(rs.getString("nom"), rs.getString("Adresse"), rs.getString("contact")));
         	
		
			
			
             }
         } catch (SQLException ex) {
             ex.printStackTrace();
         }
     
         res1.setCellValueFactory(new PropertyValueFactory<>("res1"));
		 res2.setCellValueFactory(new PropertyValueFactory<>("res2"));
		 res3.setCellValueFactory(new PropertyValueFactory<>("res3")); 
		
		listRes.setItems(data);
		
		
		
	}
	
	public void logOut(ActionEvent event)throws SQLException{
		try 
		{
			AnchorPane fxmlLoader =  FXMLLoader.load(getClass().getResource("/application/Accueil.fxml"));
			rootPane.getChildren().setAll(fxmlLoader);
			
			
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
