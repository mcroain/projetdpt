package brouillon;



/**
* @generated
*/
public class Stagiaire  {
    
    /**
    * @generated
    */
	private Integer idStagiaire;
    private String dureeStage;
    private String nvEtude;
    private String typeMission ;
    
    public Stagiaire(Integer idStagiaire, String dureeStage, String nvEtude, String typeMission) {
		super();
		this.idStagiaire = idStagiaire;
		this.dureeStage = dureeStage;
		this.nvEtude = nvEtude;
		this.typeMission = typeMission;
	}

	public Stagiaire() {
		// TODO Auto-generated constructor stub
	}

	

	public Integer getIdStagiaire() {
		return idStagiaire;
	}

	public void setIdStagiaire(Integer idStagiaire) {
		this.idStagiaire = idStagiaire;
	}

	public String getNvEtude() {
		return nvEtude;
	}

	public void setNvEtude(String nvEtude) {
		this.nvEtude = nvEtude;
	}

	public String getTypeMission() {
		return typeMission;
	}

	public void setTypeMission(String typeMission) {
		this.typeMission = typeMission;
	}

	public String getDureeStage() {
		return dureeStage;
	}

	public void setDureeStage(String dureeStage) {
		this.dureeStage = dureeStage;
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return null;
	}
    
    
    
}
