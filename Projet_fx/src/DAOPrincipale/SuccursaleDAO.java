package DAOPrincipale;

import java.sql.Connection;
import Principale.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;

/**
 * Classe DAO pour les succursales
 * @author maxime,marc-eli
 * @see DAO 
 * @see Succursale
 */
public class SuccursaleDAO extends DAO<Succursale> {

	public SuccursaleDAO() throws SQLException {
		
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	@Override
	public boolean create(Succursale obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	        
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO succursale VALUES (NULL, ?, ?,null)");
	           
	            ps.setString(1, obj.getNom());
	            ps.setString(2, obj.getLocalisation());
	                 
	            int i = ps.executeUpdate();	      
	          if(i == 1) {	      
	            return true;
	          }    
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Succursale obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM succursale WHERE idSuccursale=" + obj);
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Succursale obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE succursale SET nom=?, localisation=? WHERE idSuccursale=?");
	            ps.setString(1, obj.getNom());
	            ps.setString(2, obj.getLocalisation());
	            ps.setInt(3, obj.getIdSuccursale());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#find(int)
	 */
	public Succursale find(int id)throws SQLException {
		Succursale suc = new Succursale();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM succursale  WHERE idSuccursale=" + id);
            if(rs.next())
            {
            	suc = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return suc;
	}
	
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return La Succursale avec les valeurs de la requete
	 * @throws SQLException
	 */
	 public static Succursale getResult(ResultSet rs) throws SQLException {
	        Succursale suc = new Succursale();
	        suc.setIdSuccursale( rs.getInt("idSuccursale"));
	        suc.setNom( rs.getString("nom") );
	        suc.setLocalisation( rs.getString("localisation") );
	       
	        return suc;
	    }
	
	
	
		
}


