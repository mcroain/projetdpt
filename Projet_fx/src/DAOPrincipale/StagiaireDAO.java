package DAOPrincipale;

import java.sql.Connection;
import Principale.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;

/**
 * Classe DAO pour les stagiaires
 * @author maxime,marc-eli
 * @see DAO 
 * @see Stagiaire
 */
public class StagiaireDAO extends DAO<Stagiaire> {

	public StagiaireDAO() throws SQLException {
		
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	@Override
	public boolean create(Stagiaire obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	        	//On prepare la requete
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO stagiaire VALUES (NULL, ?, ?, ?, ?, ?)");
	            //On prend le resultat numero 1 de la requete qui est un String.
	            ps.setString(1, obj.getNom());
	            ps.setString(2, obj.getAnnee());
	            ps.setString(3, obj.getNiveauEtude());
	            ps.setString(4, obj.getTypeMissionContrat());
	            ps.setString(5, obj.getDureeStage());
	            
	            //On attribue  i l'execution de la requete
	            int i = ps.executeUpdate();
	            //si i est executable
	          if(i == 1) {
	        	  //go le fairee
	            return true;
	          }
	          //on catch un exception SQL si ya un pb avec la co a la base
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	      //return false si probleme
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Stagiaire obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM stagiaire WHERE idStagiaire=" + obj.getIdStagiaire());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Stagiaire obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE stagiaire SET nom=?, annee=?, niveauEtude=?, typeMissionContrat=?, dureeStage=? WHERE idStagiaire=" +obj.getIdStagiaire());
	            ps.setString(1, obj.getNom());
	            ps.setString(2, obj.getAnnee());
	            ps.setString(3, obj.getNiveauEtude());
	            ps.setString(4, obj.getTypeMissionContrat());
	            ps.setString(5, obj.getDureeStage());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#find(int)
	 */
	public Stagiaire find(int id)throws SQLException {
		Stagiaire sta = new Stagiaire();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stagiaire  WHERE idStagiaire=" + id);
            if(rs.next())
            {
            	sta = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return sta;
	}
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return Le Stagiaire avec les valeurs de la requete
	 * @throws SQLException
	 */
	 public static Stagiaire getResult(ResultSet rs) throws SQLException {
		 Stagiaire sta = new Stagiaire();
	        sta.setIdStagiaire( rs.getInt("idStagiaire") );
	        sta.setNom(rs.getString("nom"));
	        sta.setDureeStage( rs.getString("duree") );
	        sta.setAnnee( rs.getString("annee") );
	        sta.setNiveauEtude( rs.getString("niveauEtude") );
	        sta.setTypeMissionContrat( rs.getString("typeMissionContrat") );
	      
	       
	        return sta;
	    }
	
	
	
		
}