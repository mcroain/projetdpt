package DAOPrincipale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;
import Principale.BDDPrincipale;
import Principale.Communication;

public class CommunicationDAO extends DAO<Communication> {

	public CommunicationDAO() throws SQLException {
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO communication VALUES (NULL, ?, ?)");
	            ps.setString(1, obj.getType());
	            ps.setString(2, obj.getPlage());
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	
	@Override
	public boolean delete(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM communication WHERE idCommunication=" + obj.getIdCommunication());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	
	@Override
	public boolean update(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE communication SET type=?, plage=? WHERE idCommunication=?");
	            ps.setString(1, obj.getType());
	            ps.setString(2, obj.getPlage());
	            ps.setInt(3, obj.getIdCommunication());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}

	public Communication find(int id)throws SQLException {
		Communication com = new Communication();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication  WHERE idCommunication=" + id);
            if(rs.next())
            {
            	com = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return com;
	}
	
	public static Set getAllCommunication() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication");
            Set communication = new HashSet();
            while(rs.next())
            {
                Communication com = getResult(rs);
                communication.add(com);
            }
            return communication;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
	 public static Communication getResult(ResultSet rs) throws SQLException {
	        Communication com = new Communication();
	        com.setIdCommunication( rs.getInt("idCommunication") );
	        com.setType( rs.getString("type") );
	        com.setPlage( rs.getString("plage") );
	       
	        return com;
	    }
	
	
	
		
}


