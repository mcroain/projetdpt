package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AdminPageController {
	
	public void Manage(ActionEvent event)throws IOException 
	{
		try 
		{
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FenetreManage.fxml"));
				AnchorPane root4 = (AnchorPane) fxmlLoader.load();
				Stage stage = new Stage();
				stage.setTitle("Gestion");
				stage.setScene(new Scene(root4));
				stage.show();
				stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
}