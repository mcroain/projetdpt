package DAOPrincipale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

public class ConferenceDAO extends DAO<Conference> {

	public ConferenceDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Conference obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO conference VALUES (NULL, ?, ?,?)");
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getPublique());
	            ps.setInt(3, obj.getDuree());
	          
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public boolean delete(Conference obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM competence WHERE idCompetence=" + obj.getIdConference());
          if(i == 1) {
        return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public boolean update(Conference obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE conference SET theme=?, public=?, duree=? WHERE idConference=?");
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getPublique());
	            ps.setInt(3, obj.getDuree());
	            ps.setInt(4, obj.getIdConference());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public Conference find(int id) throws SQLException {
		Conference conf = new Conference();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM conference  WHERE idConference=" + id);
            if(rs.next())
            {
            	conf = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return conf;
	}
	
	public static Set getAllConference() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication");
            Set conference = new HashSet();
            while(rs.next())
            {
                Conference conf = getResult(rs);
                conference.add(conf);
            }
            return conference;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

	public static Conference getResult(ResultSet rs) throws SQLException {
        Conference conf = new Conference();
        conf.setIdConference( rs.getInt("idConference") );
        conf.setTheme( rs.getString("theme") );
        conf.setPublique( rs.getString("public") );
        conf.setDuree(rs.getInt("duree"));
       
        return conf;
    }
}
