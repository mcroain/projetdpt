package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.*;
import Principale.Alumni;
import Principale.Conference;
import Principale.Entreprise;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des Conferences
 * @see GestionConferences
 * @author marc-eli
 *
 */
public class FenetreConferenceController implements Initializable {

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Conference> listConf;
	@FXML
	public TableColumn<Conference, String> col_id;
	@FXML
	public TableColumn<Conference, String> col_theme;
	@FXML
	public TableColumn<Conference, String> col_duree;
	@FXML
	public TableColumn<Conference, String> col_public;
	@FXML
	public TableColumn<Conference, String> col_check;
	
	public ObservableList<Conference> data = FXCollections.observableArrayList();
	
	public TextField txt_id;
	public TextField txt_theme;
	public TextField txt_duree;
	public TextField txt_public;
	public TextField txt_rechercher;
	
	Conference conf = new Conference();
	
	

	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Conference"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute a l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Conference"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listConf.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM conference");
			while(rs.next()) {
				data.add(new Conference(rs.getInt("idConference"),
										rs.getString("theme"),
										rs.getString("public"),
										rs.getInt("duree")));									
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("idConference"));
		col_theme.setCellValueFactory(new PropertyValueFactory<>("theme"));
		col_duree.setCellValueFactory(new PropertyValueFactory<>("duree"));
		col_public.setCellValueFactory(new PropertyValueFactory<>("publique"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listConf.setItems(data);
		
		FilteredList<Conference> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(conf -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(conf.getPublique().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(conf.getTheme().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(conf.getDuree()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(String.valueOf(conf.getIdConference()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				
				else
					return false;
			});
			
				});
			SortedList<Conference> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listConf.comparatorProperty());
			listConf.setItems(sorteData);
	}
	
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Conferences.<br>
	 * Une nouvelle Conference sera cree pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cette Conference a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see ConferenceDAO.create();
	 */

	public void ajouter(ActionEvent event)throws SQLException {
		
		 conf = new Conference(null,txt_theme.getText(),
							   txt_public.getText(),Integer.parseInt(txt_duree.getText()));

		 ConferenceDAO add = new ConferenceDAO();
		 add.create(conf);
	}
	
	
	/**
	 * Methode permettant de supprimer un alumnus de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see ConferenceDAO.delete()
	 */
	
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Conference> dataRemove = FXCollections.observableArrayList();		
		 ConferenceDAO clean = new ConferenceDAO();
		 for(Conference conf : data) {
			 if(conf.getCheck().isSelected()) {
				
				 dataRemove.add(conf);
				 clean.delete(conf);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	/**
	 * Methode permettant de mettre a jour un element de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox a ete cochee seront modifies en consequence<br>
	 * @param event
	 * @throws SQLException
	 * @see ConferenceDAO.update()
	 */
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Conference> dataUpdate = FXCollections.observableArrayList();		
		 ConferenceDAO maj = new ConferenceDAO();
		 
		 
		 for(Conference conf : data) {
			 if(conf.getCheck().isSelected()) {
				 Conference alter = new Conference(conf.getIdConference(),txt_theme.getText(),txt_public.getText(),
							Integer.parseInt(txt_duree.getText()));
				 maj.update(alter);
			 }
		 }
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
}
