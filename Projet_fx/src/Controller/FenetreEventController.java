package Controller;

import java.net.URL;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.CoursDAO;
import DAOPrincipale.DAO;
import DAOPrincipale.EvenementDAO;
import Principale.Cours;
import Principale.Entreprise;
import Principale.Evenement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des Entreprises.
 * @see GestionEvent
 * @author marc-eli
 */
public class FenetreEventController implements Initializable{

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Evenement> listEvent;
	@FXML
	public TableColumn<Evenement, String> col_id;
	@FXML
	public TableColumn<Evenement, String> col_nom;
	@FXML
	public TableColumn<Evenement, String> col_somme;
	@FXML
	public TableColumn<Evenement, String> col_check;
	
	public ObservableList<Evenement> data = FXCollections.observableArrayList();
	
	public TextField txt_id;
	public TextField txt_nomE;
	public TextField txt_somme;
	public TextField txt_rechercher;
	
	Evenement even = new Evenement();
	
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Evenement"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute a l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Evenement"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listEvent.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM evenement");
			while(rs.next()) {
				data.add(new Evenement(rs.getInt("idEvenement"),
									rs.getString("nom"),
									rs.getInt("somme")));
																	
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("idEvent"));
		col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		col_somme.setCellValueFactory(new PropertyValueFactory<>("somme"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listEvent.setItems(data);
		
		FilteredList<Evenement> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(even -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				 if(String.valueOf(even.getIdEvent()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				 else if(even.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				
				else if(String.valueOf(even.getSomme()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else
					return false;
			});
			
				});
			SortedList<Evenement> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listEvent.comparatorProperty());
			listEvent.setItems(sorteData);
	}

	

	/**
	 * Methode permettant de supprimer un evenement de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see EvenementDAO.delete()
	 */
	
	@FXML
	public void remove(ActionEvent event)throws SQLException {
	
	 ObservableList<Evenement> dataRemove = FXCollections.observableArrayList();		
	 DAO<Evenement> clean = new EvenementDAO();
	 for(Evenement even : data) {
		 if(even.getCheck().isSelected()) {
			
			 dataRemove.add(even);
			 clean.delete(even);
		 }
	 }
	 data.removeAll(dataRemove);
	}
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Evenements.<br>
	 * Un nouvel Evenement sera créé pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cet Evenement a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see EvenementDAO.create();
	 */
	
	public void ajouter(ActionEvent event)throws SQLException {
		
		 even = new Evenement(null,txt_nomE.getText(),Integer.parseInt(txt_somme.getText()));
				 				

		 DAO<Evenement> add = new EvenementDAO();
		 add.create(even);
	}
	
	
	/**
	 * Methode permettant de mettre a jour un élément de la DB avec les éléments des text_field<br>
	 * Les elements dont la CheckBox a ete cochee seront modifies en consequence<br>
	 * @param event
	 * @throws SQLException
	 * @see EvenementDAO.update()
	 */
	
	
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Evenement> dataUpdate = FXCollections.observableArrayList();		
		 DAO<Evenement> maj = new EvenementDAO();
		 
		 
		 for(Evenement even : data) {
			 if(even.getCheck().isSelected()) {
				 Evenement alter = new Evenement(even.getIdEvent(),txt_nomE.getText(),Integer.parseInt(txt_somme.getText()));
							
				 maj.update(alter);
				 
			 }
		 }
	}



	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
