package Principale;


/**
* @generated
*/
public class Eleve {
    
    /**
    * @generated
    */
    private Integer idEleve;
    
    /**
    * @generated
    */
    private String nom;
    
    /**
    * @generated
    */
    private String annee;
    
    /**
    * @generated
    */
    private String niveauEtude;
    
    /**
    * @generated
    */
    private String typeMissionContrat;

	public Eleve(Integer idEleve, String nom, String annee, String niveauEtude, String typeMissionContrat) {
		super();
		this.idEleve = idEleve;
		this.nom = nom;
		this.annee = annee;
		this.niveauEtude = niveauEtude;
		this.typeMissionContrat = typeMissionContrat;
	}

	public Integer getIdEleve() {
		return idEleve;
	}

	public void setIdEleve(Integer idEleve) {
		this.idEleve = idEleve;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getNiveauEtude() {
		return niveauEtude;
	}

	public void setNiveauEtude(String niveauEtude) {
		this.niveauEtude = niveauEtude;
	}

	public String getTypeMissionContrat() {
		return typeMissionContrat;
	}

	public void setTypeMissionContrat(String typeMissionContrat) {
		this.typeMissionContrat = typeMissionContrat;
	}
    
    
}