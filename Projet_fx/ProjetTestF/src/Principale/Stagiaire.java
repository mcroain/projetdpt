package Principale;


/**
* @generated
*/
public class Stagiaire extends Eleve {
    
    /**
    * @generated
    */
    private Integer dureeStage;

	public Stagiaire(Integer idEleve, String nom, String annee, String niveauEtude, String typeMissionContrat,
			Integer dureeStage) {
		super(idEleve, nom, annee, niveauEtude, typeMissionContrat);
		this.dureeStage = dureeStage;
	}

	public Integer getDureeStage() {
		return dureeStage;
	}

	public void setDureeStage(Integer dureeStage) {
		this.dureeStage = dureeStage;
	}
    
    
    
}
