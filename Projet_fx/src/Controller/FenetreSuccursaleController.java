package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.CoursDAO;
import DAOPrincipale.DAO;
import DAOPrincipale.SuccursaleDAO;
import Principale.Cours;
import Principale.Personnel;
import Principale.Succursale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec al fenetre Admin desSuccursales
 * @author marc-eli
 * @see GestionSuccursale
 *
 */
public class FenetreSuccursaleController implements Initializable{

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Succursale> listSuccursale;
	@FXML
	public TableColumn<Succursale, String> col_id;
	@FXML
	public TableColumn<Succursale, String> col_nom;
	@FXML
	public TableColumn<Succursale, String> col_localisation;
	@FXML
	public TableColumn<Succursale, String> col_check;
	
	
	public TextField txtId;
	public TextField txtNom;
	public TextField txtLocalisation;
	public TextField txt_rechercher;

	public ObservableList<Succursale> data = FXCollections.observableArrayList();
	
	Succursale succursale = new Succursale();
	
	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Succursale"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute à l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Succursale"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listSuccursale.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM Succursale");
			while(rs.next()) {
				data.add(new Succursale(rs.getInt("idSuccursale"),
									rs.getString("nom"),
									rs.getString("localisation")));
																
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		col_id.setCellValueFactory(new PropertyValueFactory<>("idSuccursale"));
		col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		col_localisation.setCellValueFactory(new PropertyValueFactory<>("localisation"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listSuccursale.setItems(data);
		
		FilteredList<Succursale> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(succursale -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(succursale.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				else if(String.valueOf(succursale.getIdSuccursale()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(succursale.getLocalisation().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				else
					return false;
			});
			
				});
			SortedList<Succursale> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listSuccursale.comparatorProperty());
			listSuccursale.setItems(sorteData);
	}
	
	/**
	 * Methode permettant d'ajouter des elements a la BDD correspondant aux Succusrales.<br>
	 * Une nouvelles Succursale sera cree pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cette Succursale a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see SuccursaleDAO.create();
	 */
	
	public void ajouter(ActionEvent event)throws SQLException {
		
		 succursale = new Succursale(null,txtNom.getText(),txtLocalisation.getText());
				 			

		 DAO<Succursale> add = new SuccursaleDAO();
		 add.create(succursale);
	}
	

	/**
	 * Methode permettant de supprimer une succursale de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see SuccursaleDAO.delete()
	 */
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Succursale> dataRemove = FXCollections.observableArrayList();		
		 DAO<Succursale> clean = new SuccursaleDAO();
		 for(Succursale succursale : data) {
			 if(succursale.getCheck().isSelected()) {
				
				 dataRemove.add(succursale);
				 clean.delete(succursale);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	/**
	 * Methode permettant de mettre a jour un élément de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox à été cochée seront modifies en consequence<br>
	 * @param event
	 * @throws SQLException
	 * @see Succursale.update()
	 */
	
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Succursale> dataUpdate = FXCollections.observableArrayList();		
		 DAO<Succursale> maj = new SuccursaleDAO();
		 
		 
		 for(Succursale succursale : data) {
			 if(succursale.getCheck().isSelected()) {
				 Succursale alter = new Succursale(succursale.getIdSuccursale(),txtNom.getText(),txtLocalisation.getText());
							
				 maj.update(alter);
			 }
		 }
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
}
