package DAOPrincipale;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

/**
 * Classe DAO pour les competences
 * @author marc-eli
 * @see DAO 
 * @see Cours
 */
public class CoursDAO extends DAO<Cours>{

	public CoursDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	@Override
	public boolean create(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO cours VALUES (NULL, ?, ?, ?, ?)");
	            ps.setString(1, obj.getMatiere());
	            ps.setString(2, obj.getFilliere());
	            ps.setInt(3, obj.getAnnee());
	            ps.setInt(4, obj.getNombreHeure());
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }

		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM cours WHERE idCours=" + obj.getIdCours());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Cours obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE cours SET matiere=?, filliere=?, annee =?, nbHeure=? WHERE idCours="+obj.getIdCours());
	            ps.setString(1, obj.getMatiere());
	            ps.setString(2, obj.getFilliere());
	            ps.setInt(3, obj.getAnnee());
	            ps.setInt(4, obj.getNombreHeure());
	          
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#find(int)
	 */
	@Override
	public Cours find(int id) throws SQLException {
		Cours cours = new Cours();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM cours  WHERE idCours=" + id);
            if(rs.next())
            {
            	cours = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return cours;
		
	}
	
	
	
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return Le Cours avec les valeurs de la requete
	 * @throws SQLException
	 */
	
	public static Cours getResult(ResultSet rs) throws SQLException {
        Cours cours = new Cours();
        cours.setIdCours( rs.getInt("idCours") );
        cours.setMatiere( rs.getString("matiere") );
        cours.setFilliere( rs.getString("filliere") );
        cours.setAnnee( rs.getInt("annee") );
        cours.setNombreHeure( rs.getInt("nombreHeure") );
       
        return cours;
    }

}
