package Principale;

import javafx.scene.control.CheckBox;

/**
* @generated
*/
public class Succursale extends Entreprise {
    
	/**
    * @generated
    */
    private Integer idSuccursale;
	    
    /**
    * @generated
    */
    private String nom;
    
    /**
    * @generated
    */
    private String localisation;

    private CheckBox check;
    
	public Succursale(Integer idSuccursale, String nom, String localisation) {
		super();
		this.idSuccursale = idSuccursale;
		this.nom = nom;
		this.localisation = localisation;
		this.check = new CheckBox();
	}
	
	
	public Succursale() {
		
	}
	
	public Integer getIdSuccursale() {
		return idSuccursale;
	}
	
	public void setIdSuccursale(Integer idSuccursale) {
		this.idSuccursale = idSuccursale; 
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
     
	public CheckBox getCheck() {
		return check;
	}

	public void setCheck(CheckBox check) {
		this.check = check;
	}

}
