package Principale;


/**
* @generated
*/
public class Stagiaire {
	
	/**
    * @generated
    */
    private Integer idStagiaire;
	    
    /**
    * @generated
    */
    private String nom;
	    
    /**
    * @generated
    */
    private String annee;
	    
    /**
    * @generated
    */
    private String niveauEtude;
	    
    /**
    * @generated
    */
    private String typeMissionContrat;
    
    /**
    * @generated
    */
    private String dureeStage;

	public Stagiaire(Integer idStagiaire, String nom, String annee, String niveauEtude, String typeMissionContrat,
			String dureeStage) {
		super();
		this.idStagiaire = idStagiaire;
		this.nom = nom;
		this.dureeStage = dureeStage;
		this.annee = annee;
		this.niveauEtude = niveauEtude;
		this.typeMissionContrat = typeMissionContrat;
		
	}
	
	public Stagiaire() {
		
	}
	public Integer getIdStagiaire() {
		return idStagiaire;
	}

	public void setIdStagiaire(Integer idStagiaire) {
		this.idStagiaire = idStagiaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getNiveauEtude() {
		return niveauEtude;
	}

	public void setNiveauEtude(String niveauEtude) {
		this.niveauEtude = niveauEtude;
	}

	public String getTypeMissionContrat() {
		return typeMissionContrat;
	}

	public void setTypeMissionContrat(String typeMissionContrat) {
		this.typeMissionContrat = typeMissionContrat;
	}

	public String getDureeStage() {
		return dureeStage;
	}

	public void setDureeStage(String dureeStage) {
		this.dureeStage = dureeStage;
	}
    
    
    
}
