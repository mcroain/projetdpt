package DAOPrincipale;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;
import Principale.*;

/**
 * Classe DAO pour les alumni
 * @author maxime,marc-eli
 * @see DAO 
 * @see Alumni
 */
public class AlumniDAO extends DAO<Alumni> {

	public AlumniDAO() throws SQLException {
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	
	@Override
	public boolean create(Alumni obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	        	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO alumni VALUES (NULL, ?, ?, ?, null, null)");
	            ps.setString(1, obj.getNom());
	            ps.setString(2, obj.getPoste());
	            ps.setString(3, obj.getTypeMissionContrat());
	           
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	
	@Override
	public boolean delete(Alumni obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM communique_a WHERE idAlumni=" + obj.getIdAlumni());
	          if(i == 1) {
	        return true;
	          }
	          i = stmt.executeUpdate("DELETE FROM alumni WHERE idAlumni=" + obj.getIdAlumni());
	          if(i == 1) {
	  	        return true;
	  	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/*
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Alumni obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE alumni SET nom=?, annee=?, niveauEtude=?, typeMissionContrat=?, poste=?, contact=? WHERE idAlumni=?");
	            ps.setInt(1, obj.getIdAlumni());
	            ps.setString(2, obj.getNom());
	            ps.setString(3, obj.getAnnee());
	            ps.setString(4, obj.getNiveauEtude());
	            ps.setString(5, obj.getTypeMissionContrat());
	            ps.setString(6, obj.getPoste());
	            ps.setString(7, obj.getContact());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}

	/*
	 * @see DAOPrincipale.DAO#find(int)
	 */
	public Alumni find(int id)throws SQLException {
		Alumni al = new Alumni();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM alumni  WHERE idAlumni=" + id);
            if(rs.next())
            {
            	al = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return al;
	}
	
	/**
	 * 
	 * @param rs correspond a la requete SQL
	 * @return L'alumnus avec les valeurs de la requete
	 * @throws SQLException
	 */
	 public static Alumni getResult(ResultSet rs) throws SQLException {
	        Alumni al = new Alumni();
	        al.setIdAlumni( rs.getInt("idAlumni") );
	        al.setNom( rs.getString("nom") );
	        al.setAnnee( rs.getString("annee") );
	        al.setNiveauEtude( rs.getString("niveauEtude") );
	        al.setTypeMissionContrat( rs.getString("typeMissionContrat") );
	        al.setPoste( rs.getString("poste") );
	        al.setContact( rs.getString("contact") );
	       
	        return al;
	    }
	
	
	
		
}