package Principale;

import java.util.Set;

import javafx.scene.control.CheckBox;

import java.util.HashSet;

/**
* @generated
*/
public class Entreprise {
    
    
    private Integer idEntreprise;
    private String raisonSociale;
    private String denomination;
    private String adresseSiege;
    private String secteurActivite;   
    private String site; 
    private CheckBox check;
    private Set<Communication>listCom;
    private Set<Succursale>listSuc;
    private Set<Stagiaire>listStagiaire;
    private Set<Alumni>listAlumni;
    private Set<Evenement>listEvent;
    
    
    
        
	public Entreprise(Integer idEntreprise, String raisonSociale, String denomination, String adresseSiege,
			String secteurActivite, String site) {
		super();
		this.idEntreprise = idEntreprise;
		this.raisonSociale = raisonSociale;
		this.denomination = denomination;
		this.adresseSiege = adresseSiege;
		this.secteurActivite = secteurActivite;
		this.site = site;
		this.check = new CheckBox();
	}

	
	

	public Entreprise() {
	}
	
	
	


	public Integer getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Integer idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getAdresseSiege() {
		return adresseSiege;
	}

	public void setAdresseSiege(String adresseSiege) {
		this.adresseSiege = adresseSiege;
	}

	public String getSecteurActivite() {
		return secteurActivite;
	}

	public void setSecteurActivite(String secteurActivite) {
		this.secteurActivite = secteurActivite;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}


	public Set<Communication> getListCom(){
		return listCom;
	}
	
	public void setListCom(Set<Communication> listCom) {
		this.listCom = listCom;
	}

	
	public void addCommunication(Communication com){
	    this.listCom.add(com);
	}
	  
	public void removeCommunication(Communication com){
		    this.listCom.remove(com);
	}
	
	public CheckBox getCheck() {
		return check;
	}


	public void setCheck(CheckBox check) {
		this.check = check;
	}

	
	public Set<Succursale> getListSuc(){
		return listSuc;
	}
	
	public void setListSuc(Set<Succursale> listSuc) {
		this.listSuc = listSuc;
	}
	
	public void addSuccursale(Succursale suc){
	    this.listSuc.add(suc);
	}
	  
	public void removeSuccursale(Succursale suc){
		    this.listSuc.remove(suc);
	}

	public Set<Stagiaire> getListStagiaire(){
		return listStagiaire;
	}
	
	public void setListStagiaire(Set<Stagiaire> listStagiaire) {
		this.listStagiaire = listStagiaire;
	}
	
	public void addStagiaire(Stagiaire stagiaire){
	    this.listStagiaire.add(stagiaire);
	}
	  
	public void removeStagiaire(Stagiaire stagiaire){
		    this.listStagiaire.remove(stagiaire);
	}

	public Set<Alumni> getListAlumni(){
		return listAlumni;
	}
	
	public void setListAlumni(Set<Alumni> listAlumni) {
		this.listAlumni = listAlumni;
	}

	public void addAlumni(Alumni alumni){
	    this.listAlumni.add(alumni);
	}
	  
	public void removeAlumni(Alumni alumni){
		    this.listAlumni.remove(alumni);
	}
	
	public Set<Evenement> getListEvent(){
		return listEvent;
	}
	public void setListEvent(Set<Evenement> listEvent) {
		this.listEvent = listEvent;
	}
    
	public void addEvent(Evenement event){
	    this.listEvent.add(event);
	}
	  
	public void removeEvent(Evenement event){
		    this.listEvent.remove(event);
	}
	
	
	
	
	
	
	
    
}