package Connexion;

/**
 * Cette classe permet de creer l'objet "utilisateur"
 * @author marc-eli, corentin, maxime
 * @version 1.0
 */
public class Utilisateurs {

	private Integer id;
	private String pseudo;
	private String password;
	private Integer type;
	
	/**
	 * Constructeur par defaut
	 */
	public Utilisateurs() {}
	
	/**
	 * Construit un utilisateur avec un id, un pseudo, un mot de passe et un type.<br>
	 * Parametres conformes � la BDD
	 * @param id = id de l'utilisateur
	 * @param pseudo = pseudo de l'utilisateur (ou nom selon l'appellation)
	 * @param password = mot de passe de l'utilisateur
	 * @param type = type de l'utilisateur (admin ou utilisateur lambda)
	 */
	public Utilisateurs(Integer id, String pseudo, String password, Integer type) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.password = password;
		this.type = type;
	}

	/**
	 * Pour obtenir le type de l'utilisateur
	 * @return le type de l'utilisateur
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * Modifie le type de l'utilisateur avec l'argument pass� en parametre
	 * @param type = nouveau type
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * Pour obtenir l'identifiant de l'utilisateur
	 * @return l'id de l'utilisateur
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Modifie l'identifiant de l'utilisateur avec l'argument pass� en parametre
	 * @param id = nouvel identifiant
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Pour obtenir le pseudo/nom de l'utilisateur
	 * @return le nom de l'utilisateur
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Modifie le nom/pseudo de l'utilisateur avec l'argument pass� en parametre
	 * @param pseudo = nouveau nom/pseudo de l'utilisateur
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Pour obtenir le mot de passe de l'utilisateur
	 * @return le mot de passe de l'utilisateur
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Modifie le mot de passe de l'utilisateur avec l'argument pass� en parametre
	 * @param password = nouveau mot de passe
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
