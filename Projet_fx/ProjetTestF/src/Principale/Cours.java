package Principale;

import java.util.Set;
import java.util.HashSet;

/**
* @generated
*/
public class Cours {
    
    /**
    * @generated
    */
    private Integer idCours;
    
    /**
    * @generated
    */
    private String matiere;
    
    /**
    * @generated
    */
    private String filliere;
    
    /**
    * @generated
    */
    private String annee;
    
    /**
    * @generated
    */
    private Integer nombreHeure;

	public Cours(Integer idCours, String matiere, String filliere, String annee, Integer nombreHeure) {
		super();
		this.idCours = idCours;
		this.matiere = matiere;
		this.filliere = filliere;
		this.annee = annee;
		this.nombreHeure = nombreHeure;
	}

	public Cours() {
		
	}
	
	public Integer getIdCours() {
		return idCours;
	}

	public void setIdCours(Integer idCours) {
		this.idCours = idCours;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public String getFilliere() {
		return filliere;
	}

	public void setFilliere(String filliere) {
		this.filliere = filliere;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public Integer getNombreHeure() {
		return nombreHeure;
	}

	public void setNombreHeure(Integer nombreHeure) {
		this.nombreHeure = nombreHeure;
	}
    
    
}