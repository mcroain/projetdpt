package DAOPrincipale;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Connexion.Utilisateurs;
import Principale.Alumni;
import Principale.Communication;

/**
 * Classe DAO pour les communications
 * @author marc-eli
 * @see DAO 
 * @see Communication
 */
public class CommunicationDAO extends DAO<Communication> {

	public CommunicationDAO() throws SQLException {
		
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	
	@Override
	public boolean create(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO communication VALUES (NULL, ?, ?)");
	            ps.setString(1, obj.getType());
	            ps.setString(2, obj.getPlage());
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM communication WHERE idCommunication=" + obj.getIdCommunication());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/*
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Communication obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE communication SET type=?, plage=? WHERE idCommunication=?");
	            ps.setString(1, obj.getType());
	            ps.setString(2, obj.getPlage());
	            ps.setInt(3, obj.getIdCommunication());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#find(int)
	 */
	public Communication find(int id)throws SQLException {
		Communication com = new Communication();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication  WHERE idCommunication=" + id);
            if(rs.next())
            {
            	com = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return com;
	}
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return La communication avec les valeurs de la requete
	 * @throws SQLException
	 */
	
	 public static Communication getResult(ResultSet rs) throws SQLException {
	        Communication com = new Communication();
	        com.setIdCommunication( rs.getInt("idCommunication") );
	        com.setType( rs.getString("type") );
	        com.setPlage( rs.getString("plage") );
	       
	        return com;
	    }
	
	
	
		
}


