package DAOPrincipale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.BDDPrincipale;
import Principale.Communication;
import Principale.Entreprise;

public class EntrepriseDAO extends DAO<Entreprise> {
	
	
	
	public EntrepriseDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	

	@Override
	public boolean create(Entreprise obj)throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO entreprise VALUES (NULL, ?, ?, ?, ?, ?)");
            ps.setString(1, obj.getRaisonSocial());
            ps.setString(2, obj.getDenomination());
            ps.setString(3, obj.getAdresseSiege());
            ps.setString(4, obj.getSecteurActivite());
            ps.setString(5, obj.getSite());
            
            
            int i = ps.executeUpdate();
          if(i == 1) {
            return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public boolean delete(Entreprise obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM entreprise WHERE idEntreprise=" + obj.getIdEntreprise());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	
	@Override
	public boolean update(Entreprise obj)throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE entreprise SET raisonSocial=?, denomination=?, adresseSiege=?, secteurActivite=?, site=? WHERE idEntreprise=?");
	            ps.setString(1, obj.getRaisonSocial());
	            ps.setString(2, obj.getDenomination());
	            ps.setString(3, obj.getAdresseSiege());
	            ps.setString(4, obj.getSecteurActivite());
	            ps.setString(5, obj.getSite());
	            ps.setInt(6, obj.getIdEntreprise());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	
	@Override
	public Entreprise find(int id) {
		Entreprise cie = new Entreprise();
		
		try {			
            Statement stmt = this.connect.createStatement();         
            ResultSet rs = stmt.executeQuery("SELECT * FROM entreprise  WHERE idEntreprise=" + id);         
            if(rs.next())
            {
            	cie = getResult(rs);
            	
            	rs = this.connect.createStatement().executeQuery("SELECT type, plage FROM Communication c INNER JOIN Communique co"
            													+ "ON c.idCommunication = co.idCommunication INNER JOIN Entreprise e"
            													+ "ON co.idEntreprise = e.idEntreprise"
            													+ "AND idEntreprise =" +id);
            	       
            	CommunicationDAO com = new CommunicationDAO();
            	        	
            	while(rs.next()) 
            		
            		cie.addCommunication(com.find(rs.getInt("c.idCommunication")));            	            
            	
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return cie ;
	
	}
	
	
	
	
	public static Set getAllEntreprise() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM entreprise");
            Set entreprise = new HashSet();
            while(rs.next())
            {
                Entreprise cie = getResult(rs);
                entreprise.add(cie);
            }
            return entreprise;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
	
	public static Entreprise getResult(ResultSet rs) throws SQLException {
        Entreprise cie = new Entreprise();
        cie.setIdEntreprise( rs.getInt("idEntreprise") );
        cie.setRaisonSocial( rs.getString("raisonSocial") );
        cie.setDenomination( rs.getString("denomination") );
        cie.setAdresseSiege(rs.getString("adresseSiege"));
        cie.setSecteurActivite( rs.getString("secteurActivite") );
        cie.setSite(rs.getString("site"));
       
        return cie;
	}
	
	
	
	
	
	
}
