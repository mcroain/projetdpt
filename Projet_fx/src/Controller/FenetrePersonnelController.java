package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.CoursDAO;
import DAOPrincipale.DAO;
import DAOPrincipale.PersonnelDAO;
import Principale.Cours;
import Principale.Evenement;
import Principale.Personnel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * Cette classe permet d'interagir avec la fenetre Admin des Entreprises.
 * @see Gestion
 * @author marc-eli
 */

public class FenetrePersonnelController implements Initializable{

	@FXML
	public AnchorPane rootPane;
	@FXML
	public TableView<Personnel> listPers;
	@FXML
	public TableColumn<Personnel, String> col_id;
	@FXML
	public TableColumn<Personnel, String> col_nom;
	@FXML
	public TableColumn<Personnel, String> col_fonction;
	@FXML
	public TableColumn<Personnel, String> col_entreprise;
	@FXML
	public TableColumn<Personnel, String> col_contact;
	@FXML
	public TableColumn<Personnel, String> col_check;
	
	public ObservableList<Personnel> data = FXCollections.observableArrayList();
	
	public TextField txt_id;
	public TextField txt_nom;
	public TextField txt_fonction;
	public TextField txt_entreprise;
	public TextField txt_contact;
	public TextField txt_rechercher;
	
	Personnel pers = new Personnel();
	


	/**
	 * Methode permettant de remplir la TableView avec les résultats de la BDD compris dans la table "Personnel"<br>
	 * On n'oubliera pas de "clear" la liste pour ne pas avoir de redondance en cas de nouvelle recherche<br>
	 * Cette methode s'execute à l'aide du bouton "get" de la page. 
	 * @param event
	 * @throws SQLException
	 * @see Class "Personnel"
	 */
	public void get(ActionEvent event)throws SQLException {
		
		data.clear();
		listPers.getItems().clear();
		
		Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT p.idPersonnel, p.nom, p.fonction,e.denomination,cp.plage\r\n" + 
					"FROM personnel p \r\n" + 
					"LEFT OUTER JOIN communique_p cp\r\n" + 
					"ON p.idPersonnel = cp.idPersonnel\r\n" + 
					"LEFT OUTER JOIN communication c\r\n" + 
					"ON c.idCommunication = cp.idCommunication\r\n" + 
					"INNER JOIN entreprise e\r\n" + 
					"ON e.idEntreprise = p.idEntreprise");
			while(rs.next()) {
				data.add(new Personnel(rs.getInt("p.idPersonnel"),
									rs.getString("p.nom"),
									rs.getString("p.fonction"),
									rs.getString("e.denomination"),
									rs.getString("cp.plage"),
													 null));									
										
			}
		}
		catch (SQLException ex) {
            ex.printStackTrace();
        }
		
		col_id.setCellValueFactory(new PropertyValueFactory<>("idPersonnel"));
		col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		col_fonction.setCellValueFactory(new PropertyValueFactory<>("fonction"));
		col_entreprise.setCellValueFactory(new PropertyValueFactory<>("entreprise"));
		col_contact.setCellValueFactory(new PropertyValueFactory<>("plage"));
		col_check.setCellValueFactory(new PropertyValueFactory<>("check"));
		
		listPers.setItems(data);
		
		FilteredList<Personnel> filterData = new FilteredList<>(data, b -> true);
		
		txt_rechercher.textProperty().addListener((observable, oldValue,newValue) -> {
			filterData.setPredicate(pers -> {
				
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(String.valueOf(pers.getIdPersonnel()).indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(pers.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}					
				else if(pers.getPlage().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else if(pers.getFonction().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				else
					return false;
			});
			
				});
			SortedList<Personnel> sorteData = new SortedList<>(filterData);
			sorteData.comparatorProperty().bind(listPers.comparatorProperty());
			listPers.setItems(sorteData);
	}
	
	
	/**
	 * Methode permettant d'ajouter des elements à la BDD correspondant aux membres du personnel.<br>
	 * Un nouveau membre sera cree pour lui attribuer les valeurs des TextField<br>
	 * On utilisera le DAO correspodant pour ensuite ajouter cette personne a la DB<br>
	 * @param event
	 * @throws SQLException
	 * @see PersonnelDAO.create();
	 */
	public void ajouter(ActionEvent event)throws SQLException {

		 Connection con = BDDPrincipale.getConnection();
		 pers = new Personnel(null,txt_nom.getText(),txt_fonction.getText(),null);
		
		 try {				
				ResultSet rs = con.createStatement().executeQuery("SELECT idEntreprise FROm entreprise WHERE denomination ='" +txt_entreprise.getText()+"'");
				while(rs.next()) {
						pers.setIdEntreprise(rs.getInt("idEntreprise"));							
											
				}
			}
			catch (SQLException ex) {
	            ex.printStackTrace();
	        }	 			
		 
		 
		 
		
		 DAO<Personnel> add = new PersonnelDAO();
		 add.create(pers);
	}
	

	/**
	 * Methode permettant de supprimer un membre du Personnel de la DB.<br>
	 * On se servira de CheckBox et du bouton delete pour supprimer les lignes correspondant aux cases cochées
	 * On utilisera également une autre ObservableList pour attribuer les éléments de la premiere qui doivent etre supprimés.
	 * @param event
	 * @throws SQLException
	 * @see PersonnelDAO.delete()
	 */
	@FXML
	public void remove(ActionEvent event)throws SQLException {
		
		 ObservableList<Personnel> dataRemove = FXCollections.observableArrayList();		
		 DAO<Personnel> clean = new PersonnelDAO();
		 for(Personnel pers : data) {
			 if(pers.getCheck().isSelected()) {
				
				 dataRemove.add(pers);
				 clean.delete(pers);
			 }
		 }
		 data.removeAll(dataRemove);
	}
	
	
	/**
	 * Methode permettant de mettre a jour un element de la DB avec les elements des text_field<br>
	 * Les elements dont la CheckBox à ete cochee seront modifies en consequence<br>
	 * @param event
	 * @throws SQLException
	 * @see EvenementDAO.update()
	 */
	
	public void update(ActionEvent event)throws SQLException {
		 ObservableList<Personnel> dataUpdate = FXCollections.observableArrayList();		
		 DAO<Personnel> maj = new PersonnelDAO();
		 
		 
		 for(Personnel pers : data) {
			 if(pers.getCheck().isSelected()) {
				 Personnel alter = new Personnel(pers.getIdPersonnel(),txt_nom.getText(),txt_fonction.getText(),
												pers.getIdEntreprise());
				 maj.update(alter);
				 
			 }
		 }
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
