package test;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import DAOPrincipale.BDDPrincipale;
import DAOPrincipale.CoursDAO;
import DAOPrincipale.DAO;
import DAOPrincipale.EntrepriseDAO;
import Principale.Cours;
import Principale.Entreprise;

import static org.junit.Assert.*;

public class DAOTest {
	
	@Test
	public void testCoursDAO() throws SQLException {

	  DAO<Cours> cours    = new CoursDAO();
	  Cours test = new Cours(null, "tester", "autreTest", null, null);
	  Cours requete = new Entreprise(null, null, null, null, null, null);
	    
	  
	  
	  cours.create(test);
	  
	  
	  
	  Connection con = BDDPrincipale.getConnection();
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM cours WHERE matiere =" + test.getMatiere()
																				+ "AND nbHeure =" +test.getNombreHeure()
													);
			while(rs.next()) {
				 requete.setIdCours( rs.getInt("idCours") );
			        requete.setMatiere( rs.getString("matiere") );
			        requete.setFilliere( rs.getString("filliere") );
			        requete.setAnnee( rs.getInt("annee") );
			        requete.setNombreHeure( rs.getInt("nombreHeure") );							
										
			}
		}
		catch (SQLException ex) {
          ex.printStackTrace();
      }
	  
		
		
		
		assertEquals(requete.getMatiere(),test.getMatiere());
		
		
		
		
		
		test = new Cours(requete.getIdCours(), "changement", "autreTest",null,null );
		cours.update(test);
		
		
		
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM cours WHERE idCours="+test.getIdCours());
																				
													
			while(rs.next()) {
				 requete.setIdCours( rs.getInt("idCours") );
			        requete.setMatiere( rs.getString("matiere") );
			        requete.setFilliere( rs.getString("filliere") );
			        requete.setAnnee( rs.getInt("annee") );
			        requete.setNombreHeure( rs.getInt("nombreHeure") );							
										
			}
		}
		catch (SQLException ex) {
          ex.printStackTrace();
      }
		
		assertEquals(requete.getMatiere(),test.getMatiere());
		
		
		cours.delete(test);
		
		try {
			
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM cours WHERE idCours="+test.getIdCours());
																				
													
			while(rs.next()) {
				 requete.setIdCours( rs.getInt("idCours") );
			        requete.setMatiere( rs.getString("matiere") );
			        requete.setFilliere( rs.getString("filliere") );
			        requete.setAnnee( rs.getInt("annee") );
			        requete.setNombreHeure( rs.getInt("nombreHeure") );							
										
			}
		}
		catch (SQLException ex) {
          ex.printStackTrace();
      }
		
		
		assertNull(requete.getIdCours(),test.getIdCours());
		
	}
	
	
	
}
