package DAOPrincipale;



import java.sql.*;

import Principale.BDDPrincipale;

public abstract class DAO<T> {
  protected Connection connect = null;
   
  public DAO()throws SQLException{
    this.connect = BDDPrincipale.getConnection();
  }
   
  /**
  * M�thode de cr�ation
  * @param obj
  * @return boolean 
  */
  public abstract boolean create(T obj)throws SQLException;

  /**
  * M�thode pour effacer
  * @param obj
  * @return boolean 
  */
  public abstract boolean delete(T obj)throws SQLException;

  /**
  * M�thode de mise � jour
  * @param obj
  * @return boolean
  */
  public abstract boolean update(T obj)throws SQLException;

  /**
  * M�thode de recherche des informations
  * @param id
  * @return T
  */
  public abstract T find(int id)throws SQLException;
}