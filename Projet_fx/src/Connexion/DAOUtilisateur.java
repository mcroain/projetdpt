package Connexion;
import java.sql.*;
import java.util.*;

import javafx.scene.control.Label;
/**
 * Cette classe sert a interagir avec la table "utilisateur" de la BDD.
 * @author marc-eli, corentin, maxime
 * @version 1.0
 */
public class DAOUtilisateur {

	 	
	    
	    
	
		/** Methode renvoyant les infos d'un utilisateur selon son id entre en parametre.
		 * @param id = L'id de l'Utilisateur
		 * @return = return le resultat de la requete
		 * @throws SQLException
		 * @see getConnection
		 * @see getResult
		 */
	    public static Utilisateurs getUtilisateur(int id) throws SQLException {
	        Connection connection = BDDConnexion.getConnection();
	            try {
	                Statement stmt = connection.createStatement();
	                ResultSet rs = stmt.executeQuery("SELECT * FROM utilisateur  WHERE idUtilisateur=" + id);
	                if(rs.next())
	                {
	                	return getResult(rs);
	                }
	            } catch (SQLException ex) {
	                ex.printStackTrace();
	            }
	        return null;
	    }
	    
	   
	    
	    /** Methode permettant d'ajouter un utilisateur dans la BDD.
	     * @param utilisateur = les caractéristiques d'un objet 'Utilisateurs'
	     * @return true si l'ajout est un succes / false si il ya eu un probleme.
	     * @throws SQLException
	     * @see getConnection
	     */
	    public static boolean create(Utilisateurs utilisateur) throws SQLException {
	        Connection connection = BDDConnexion.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO utilisateur VALUES (NULL, ?, ?, ?)");
	            ps.setString(1, utilisateur.getNom());
	            ps.setString(2, utilisateur.getPassword());
	            ps.setInt(3, utilisateur.getIdType());
	            
	            int i = ps.executeUpdate();
	          if(i == 1) {
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        return false;
	    }
	    
	   
	    /** Methode permettant de supprimer un utilisateur de la BDD en prenant en parametre son ID.
	     * @param id = id De l'utilisateur
	     * @return true si la suppression est un succes/false si c'est un echec.
	     * @throws SQLException
	     * @see getConnection
	     */
	    public static boolean remove(Utilisateurs user) throws SQLException {
	       
	        Connection connection = BDDConnexion.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            int i = stmt.executeUpdate("DELETE FROM utilisateur WHERE idUtilisateur=" + user.getId());
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        return false;
	    }
	    
	    
	    
	    /** Methode permettant de modifier les attributs d'un utilisateur de la BDD
	     * @param user = les caracteristiques d'un obejt 'Utilisateurs'
	     * @return true si la maj est un succes/false si c'est un echec.
	     * @throws SQLException
	     * @see getConnection
	     */
	    public static boolean update(Utilisateurs user)throws SQLException {
	     
	        Connection connection = BDDConnexion.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE utilisateur SET name=?, pass=? WHERE id=" + user.getId());
	            ps.setString(1, user.getNom());
	            ps.setString(2, user.getPassword());
	            ps.setInt(3, user.getId());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        return false;
	    }
	    
	    
	    
	    /**Methode qui fait la requete de tous les elements de la table 'utilisateurs'. <br>
	     * On fait appel a la methode getResult pour obtenir les elements d'un utilisateur. 
	     * @return
	     * @throws SQLException
	     * @see getConnection
	     * @see getResult
	     */
	    public static Set getAllUtilisateur() throws SQLException {
	       
	        Connection connection = BDDConnexion.getConnection();
	        try {
	            Statement stmt = connection.createStatement();
	            ResultSet rs = stmt.executeQuery("SELECT * FROM utilisateur");
	            Set utilisateurs = new HashSet();
	            while(rs.next())
	            {
	                Utilisateurs user = getResult(rs);
	                utilisateurs.add(user);
	            }
	            return utilisateurs;
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
	        return null;
	    }
	    
	    
	    
	    
	    /**Methode generique permettant de creer un objet utilisateur, afin de lui attribuer les attributs pris de la BDD.
	     * @param rs = ResultatSet = resultat obtenu (requete)
	     * @return l'user avec les attibuts modifié par la requete.
	     * @throws SQLException
	     */
	    public static Utilisateurs getResult(ResultSet rs) throws SQLException {
	        Utilisateurs user = new Utilisateurs();
	        user.setId( rs.getInt("u.idUtilisateur") );
	        user.setNom( rs.getString("u.nom") );
	        user.setPassword( rs.getString("u.password") );
	        user.setType(rs.getString("t.nom"));
	        user.setIdType(rs.getInt("u.idType"));
	       
	        return user;
	    }

	      // Methode pouvant etre utile pour la connexion (comparer login + pwd). A check si pas deja de fonction integree.)
	     	
	    /** Methode prenant en parametre le pseudo + le mot de passe de l'utilisateur<br>
	     *  Si la requete r騏ssie, elle renvoie les attributs de l'user de la BDD.<br>
	     *  Si la requete echoue, un exception de Type sql est renvoyee et la requete echoue.
	     * @param user = pseudo de l'utilisateur
	     * @param pass = mot de passe de l'utilisateur
	     * @return
	     * @throws SQLException
	     * @see getConnection
	     * @see getResult
	     */
	    public static Utilisateurs checkConnect(Utilisateurs user, Label Label) throws SQLException {
	 
	        Connection connection = BDDConnexion.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("SELECT * FROM utilisateur WHERE nom=? AND password=?");
	            ps.setString(1, user.getNom());
	            ps.setString(2, user.getPassword());
	            ResultSet rs = ps.executeQuery();
	            if(rs.next())
	            {
	            	Label.setText("Connecté");
	            		 user.setId( rs.getInt("idUtilisateur") );
	         	        user.setNom( rs.getString("nom") );
	         	        user.setPassword( rs.getString("password") );
	         	        user.setType(rs.getString("nom"));
	         	        user.setIdType(rs.getInt("idType"));
		            
	            }
	           
	            else {
	            	Label.setText("Erreur lors de la saisie des ID");
	            }
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	            Label.setText("Probleme lors de la connexion");
	        }
	        
	        return user;
	    }
	    
}
