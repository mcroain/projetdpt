package Principale;

import java.util.Set;

import javafx.scene.control.CheckBox;

import java.util.HashSet;

/**
* @generated
*/
public class Evenement {
    
    /**
    * @generated
    */
    private Integer idEvent;
    
    /**
    * @generated
    */
    private String nom;
    
    /**
    * @generated
    */
    private Integer somme;
    
    private CheckBox check;

	public Evenement(Integer idEvent, String nom, Integer somme) {
		super();
		this.idEvent = idEvent;
		this.nom = nom;
		this.somme = somme;
		this.check = new CheckBox();
	}

	

	public Evenement() {
		
	}
	
	public Integer getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getSomme() {
		return somme;
	}

	public void setSomme(Integer somme) {
		this.somme = somme;
	}
    
	public CheckBox getCheck() {
		return check;
	}

	public void setCheck(CheckBox check) {
		this.check = check;
	}
    
   
}
