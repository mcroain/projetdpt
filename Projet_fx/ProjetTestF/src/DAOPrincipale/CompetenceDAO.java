package DAOPrincipale;


import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

public class CompetenceDAO extends DAO<Competence> {

	public CompetenceDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Competence obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO competence VALUES (NULL, ?, ?)");
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getOutil());
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public boolean delete(Competence obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM competence WHERE idCompetence=" + obj.getIdCompetence());
          if(i == 1) {
        return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	@Override
	public boolean update(Competence obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE competence SET theme=?, outil=? WHERE idCompetence=?");
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getOutil());
	            ps.setInt(3, obj.getIdCompetence());
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	@Override
	public Competence find(int id) throws SQLException {
		Competence comp = new Competence();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM competence  WHERE idCompetence=" + id);
            if(rs.next())
            {
            	comp = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return comp;
	}

	
	public static Set getAllCompetence() throws SQLException {
	       
        Connection connection = BDDConnexion.getConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM communication");
            Set competence = new HashSet();
            while(rs.next())
            {
                Competence comp = getResult(rs);
                competence.add(comp);
            }
            return competence;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
	
	public static Competence getResult(ResultSet rs) throws SQLException {
        Competence comp = new Competence();
        comp.setIdCompetence( rs.getInt("idCompetence") );
        comp.setTheme( rs.getString("theme") );
        comp.setOutil( rs.getString("outil") );
       
        return comp;
    }
}
