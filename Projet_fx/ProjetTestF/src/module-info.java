module ProjectGraphique {
	exports application;

	requires javafx.controls;
	requires javafx.media;
	requires javafx.swing;
	
	requires javafx.web;
	requires javafx.base;
	requires javafx.fxml;
	requires javafx.graphics;
	requires java.sql;
}