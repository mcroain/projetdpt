package Principale;

import java.util.Set;


import javafx.scene.control.CheckBox;

import java.util.HashSet;

/**
* @generated
*/
public class Cours {
    
    private Integer idCours;   
    private String matiere;  
    private String filliere;       
    private Integer annee;    
    private Integer nombreHeure;   
    private CheckBox check;

	public Cours(Integer idCours, String matiere, String filliere, Integer annee, Integer nombreHeure) {
		super();
		this.idCours = idCours;
		this.matiere = matiere;
		this.filliere = filliere;
		this.annee = annee;
		this.nombreHeure = nombreHeure;
		this.check = new CheckBox();
	}

	

	public Cours() {
		
	}
	
	public Integer getIdCours() {
		return idCours;
	}

	public void setIdCours(Integer idCours) {
		this.idCours = idCours;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public String getFilliere() {
		return filliere;
	}

	public void setFilliere(String filliere) {
		this.filliere = filliere;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public Integer getNombreHeure() {
		return nombreHeure;
	}

	public void setNombreHeure(Integer nombreHeure) {
		this.nombreHeure = nombreHeure;
	}
    
	public CheckBox getCheck() {
		return check;
	}

	public void setCheck(CheckBox check) {
		this.check = check;
	}
    
}