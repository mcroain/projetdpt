package Controller;

import java.io.*;

import java.net.URL;
import java.sql.SQLException;

import javafx.fxml.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.util.*;

import Connexion.Utilisateurs;

public class PagedeCoController implements Initializable{
	
	public TextField txtUserName;
	public TextField txtPass;
	public Label connectStatus;
	
	@FXML
	public AnchorPane pageCo;
	
	
	public void signUp(ActionEvent event)throws IOException 
	{
		try 
		{
			
			
		
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("InscriptionCo.fxml"));
				AnchorPane root2 = (AnchorPane) fxmlLoader.load();
				Stage stage = new Stage();
				stage.setTitle("Sign Up");
				stage.setScene(new Scene(root2));
				stage.show();
				stage.setResizable(false);
		} catch(Exception e) 
		{
			System.out.println("Can't load new window");
		}

	}
	
	public void login(ActionEvent actionEvent) throws SQLException {
		Utilisateurs check = new Utilisateurs(null,txtUserName.getText(),txtPass.getText(),null,null);
		
		Utilisateurs connecte = Connexion.DAOUtilisateur.checkConnect( check, connectStatus );
		System.out.println(connecte.getIdType());
		
		if(connecte.getIdType() == 1) {
			try 
			{
				AnchorPane fxmlLoader =  FXMLLoader.load(getClass().getResource("/application/AdminPage.fxml"));
				pageCo.getChildren().setAll(fxmlLoader);
				
				
			} catch(Exception e) 
			{
				System.out.println("Can't load new window");
			}
		}
		
		
		else if(connecte.getIdType() == 2) {
			
			try 
			{
				AnchorPane fxmlLoader =  FXMLLoader.load(getClass().getResource("/application/UserPage.fxml"));
				pageCo.getChildren().setAll(fxmlLoader);
				
				
			} catch(Exception e) 
			{
				System.out.println("Can't load new window");
			}
		}
		
		else{}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}