package Principale;

import java.util.Set;

/**
* @generated
*/
public class Personnel {

	
    private Integer idPersonnel;
    private String nom;   
    private String fonction;
	private Integer idEntreprise;
    private Set<Communication>listCom;
    private Set<Competence>listComp;
    private Set<Conference>listConf;
    private Set<Cours>listCours;
    private Set<Entreprise>listCie;
    
   

	public Personnel(Integer idPersonnel, String nom, Integer idEntreprise) {
		super();
		this.idPersonnel = idPersonnel;
		this.nom = nom;
		this.idEntreprise = idEntreprise;
	}
	
	public Personnel() {
		
	}

	public Integer getIdPersonnel() {
		return idPersonnel;
	}

	public void setIdPersonnel(Integer idPersonnel) {
		this.idPersonnel = idPersonnel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	
	public Integer getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Integer idEntreprise) {
		this.idEntreprise = idEntreprise;
	}
    
	public Set<Communication> getListCom(){
		return listCom;
	}
	
	public void setListCom(Set<Communication> listCom) {
		this.listCom = listCom;
	}

	
	public void addCommunication(Communication com){
	    this.listCom.add(com);
	}
	  
	public void removeCommunication(Communication com){
		    this.listCom.remove(com);
	}
	
	public Set<Competence> getListComp(){
		return listComp;
	}
	
	public void setListComp(Set<Competence> listComp) {
		this.listComp = listComp;
	}

	
	public void addCompetence(Competence comp){
	    this.listComp.add(comp);
	}
	  
	public void removeCompetence(Competence comp){
		    this.listComp.remove(comp);
	}
	
	public Set<Conference> getListConf(){
		return listConf;
	}
	
	public void setListConf(Set<Conference> listConf) {
		this.listConf = listConf;
	}

	
	public void addConference(Conference conf){
	    this.listConf.add(conf);
	}
	  
	public void removeConference(Conference conf){
		    this.listConf.remove(conf);
	}
	
	public Set<Cours> getListCours(){
		return listCours;
	}
	
	public void setListCours(Set<Cours> listCours) {
		this.listCours = listCours;
	}

	
	public void addCours(Cours cours){
	    this.listCours.add(cours);
	}
	  
	public void removeCours(Cours cours){
		    this.listCours.remove(cours);
	}
	
	public Set<Entreprise> getListCie(){
		return listCie;
	}
	
	public void setListCie(Set<Entreprise> listCie) {
		this.listCie = listCie;
	}

	
	public void addEntreprise(Entreprise cie){
	    this.listCie.add(cie);
	}
	  
	public void removeEntreprise(Entreprise cie){
		    this.listCie.remove(cie);
	}
}