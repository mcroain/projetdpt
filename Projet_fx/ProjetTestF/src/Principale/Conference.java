package Principale;

import java.util.Set;
import java.util.HashSet;

/**
* @generated
*/
public class Conference {
    
   
    private Integer idConference;
    private String theme;
    private String publique;
    private Integer duree;

	public Conference(Integer idConference, String theme, String publique, Integer duree) {
		super();
		this.idConference = idConference;
		this.theme = theme;
		this.publique = publique;
		this.duree = duree;
	}

	
	public Conference() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdConference() {
		return idConference;
	}

	public void setIdConference(Integer idConference) {
		this.idConference = idConference;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getPublique() {
		return publique;
	}

	public void setPublique(String publique) {
		this.publique = publique;
	}

	public Integer getDuree() {
		return duree;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}
    
    
    
}