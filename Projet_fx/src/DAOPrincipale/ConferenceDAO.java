package DAOPrincipale;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import Connexion.BDDConnexion;
import Principale.*;

/**
 * Classe DAO pour les competences
 * @author marc-eli
 * @see DAO 
 * @see Conference
 */
public class ConferenceDAO extends DAO<Conference> {

	public ConferenceDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @see DAOPrincipale.DAO#create(java.lang.Object)
	 */
	@Override
	public boolean create(Conference obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try { 	
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO conference VALUES (NULL, ?, ?,?)");
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getPublique());
	            ps.setInt(3, obj.getDuree());
	          
	            int i = ps.executeUpdate();
	          if(i == 1) {    
	            return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/*
	 * @see DAOPrincipale.DAO#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Conference obj) throws SQLException {
		Connection connection = BDDPrincipale.getConnection();
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM conference WHERE idConference=" + obj.getIdConference());
          if(i == 1) {
        return true;
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Conference obj) throws SQLException {
		 Connection connection = BDDPrincipale.getConnection();
	        try {
	            PreparedStatement ps = connection.prepareStatement("UPDATE conference SET theme=?, public=?, duree=? WHERE idConference="+obj.getIdConference());
	            ps.setString(1, obj.getTheme());
	            ps.setString(2, obj.getPublique());
	            ps.setInt(3, obj.getDuree());
	         
	            int i = ps.executeUpdate();
	          if(i == 1) {
	        return true;
	          }
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        }
		return false;
	}

	/**
	 * @see DAOPrincipale.DAO#find(int)
	 */
	@Override
	public Conference find(int id) throws SQLException {
		Conference conf = new Conference();
		
		try {
            Statement stmt = this.connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM conference  WHERE idConference=" + id);
            if(rs.next())
            {
            	conf = getResult(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    return conf;
	}
	
	/**
	 * @param rs correspond a la requete SQL
	 * @return La Conference avec les valeurs de la requete
	 * @throws SQLException
	 */

	public static Conference getResult(ResultSet rs) throws SQLException {
        Conference conf = new Conference();
        conf.setIdConference( rs.getInt("idConference") );
        conf.setTheme( rs.getString("theme") );
        conf.setPublique( rs.getString("public") );
        conf.setDuree(rs.getInt("duree"));
       
        return conf;
    }
}
